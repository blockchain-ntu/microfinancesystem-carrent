import React, { createContext, useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// Create context and set default values.
const UserContext = createContext({
	user: null,
	login: () => {},
});

export const UserContextProvider = ({ children }) => {
	const router = useRouter();

	const users = [
		// {
		// 	name: 'Leonard Hofstadter',
		// 	role: 'broker',
		// 	color: '#87d068',
		// },
		// {
		// 	name: 'Sheldon Cooper',
		// 	role: 'bank',
		// 	color: '#8193E7',
		// },
		// {
		// 	name: 'Howard Wolowitz',
		// 	role: 'insurance',
		// 	color: '#CCDAD5',
		// },
		// {
		// 	name: 'Guest',
		// 	role: 'guest',
		// 	color: '#8193E7',
		// },
		{
			name: 'Rajesh Koothrappali',
			role: 'borrower',
			color: '#F3D377',
		},
		{
			name: 'Mr.Simple',
			role: 'car_company',
			color: '#4257f5',
		}
	];

	const [user, setUser] = useState(users[1]);

	useEffect(() => {
		// if (user.role === 'broker') {
		// 	router.push('/common/transfer');
		// } else if (user.role === 'bank') {
		// 	router.push('/common/loans');
		// } else if (user.role === 'insurance') {
		// 	router.push('/common/policies');
		// } else if (user.role === 'guest') {
		// 	router.push('/guest/register-broker');
		// } 
		if (user.role === 'borrower') {
			router.push('/borrower/rent');
		} else if (user.role === 'car_company') {
			router.push('/car_company/add-cars');
		} 
	}, [user]); // useEffect will triggered every time user state change.

	const login = (role) => {
		// if (role === 'broker') {
		// 	setUser(users[0]);
		// } else if (role === 'bank') {
		// 	setUser(users[1]);
		// } else if (role === 'insurance') {
		// 	setUser(users[3]);
		// } else if (role === 'guest') {
		// 	setUser(users[5]);
		// }
		if (role === 'borrower') {
			setUser(users[0]);
		} else if (role === 'car_company') {
			setUser(users[1]);
		} 
	};

	const context = { user, login };

	return (
		<UserContext.Provider value={context}>
			{children}
		</UserContext.Provider>
	);
};

export default UserContext;
