import React, { useState, useContext, useEffect } from 'react';
import { Table, Form, InputNumber, Input, Card, Modal, Button, message, Space } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';
import { getApi, patchApi, deleteApi } from '../../util/fetchApi';
import UserContext from '../../stores/userContext';
import SmartContractContext from '../../stores/smartContractContext';

function CarsTable({ toggleCar }) {
	const { user } = useContext(UserContext);
	const [isModalVisible, setIsModalVisible] = useState(false);
	const [data, setData] = useState([]);
	const { RentalContract } = useContext(SmartContractContext);

	const { confirm } = Modal;

	const [carId, setCarId] = useState('');
	const [model, setModel] = useState('');
	const [licensePlate, setLicensePlate] = useState('');
	const [pricePerDay, setPricePerDay] = useState('');

	const fetchCars = async () => {
		try {
			const response = await RentalContract.methods.getRentals().call();
			
			const cars = await getApi({
				url: 'cars',
			});

			setData([]);
			for (let i = 0; i < cars.length; i++) {
				let isAvailable = "Yes";
				for(let j=0; j < response.length; j++) {	
					if(cars[i]._id === response[j].carId) {
						isAvailable = "No";
					}
				}
				const row = {
					key: cars[i]._id,
					carId: cars[i]._id,
					model: cars[i].model,
					licensePlate: cars[i].licensePlate,
					pricePerDay: cars[i].pricePerDay,
					isAvailable: isAvailable
				};
				
				setData((prev) => {
					return [...prev, row];
				});
				
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Cars');
		}
	};

	const fetchCarById = async (carId) => {
		try {
			const response = await getApi({
				url: 'cars/' + carId,
			});

			const car = await response;
			setCarId(car._id);
			setModel(car.model);
			setLicensePlate(car.licensePlate);
			setPricePerDay(car.pricePerDay);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Car');
		}
	};

	const showModal = (value) => {
		fetchCarById(value);
		setIsModalVisible(true);
	};

	const deleteCar = (carId) => {
		confirm({
			icon: <CloseCircleOutlined style={{ color: 'red' }} />,
			content: `Delete Car ${carId}`,
			onOk: async () => {
				try {
					const response = await deleteApi({
						url: 'cars/' + carId,
					});
					if (response.status === 200) {
						await message.success('Sucsessfully delete the car');
						fetchCars();
					} else {
						message.error('Error occured while deleting car');
					}
				} catch (err) {
					console.log(err);
					message.error('Error occured while deleting car');
				}
			},
			onCancel() {
				console.log('Cancel');
			},
		});
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'carId',
			render: text => text,
		},
		{
			title: 'Model',
			dataIndex: 'model'
		},
		{
			title: 'LicensePlate',
			dataIndex: 'licensePlate'
		},
		{
			title: 'Price Per Day',
			dataIndex: 'pricePerDay'
		},
		{
			title: 'Available Status',
			dataIndex: 'isAvailable'
		}
	];

	if (user.role === 'car_company') {
		columns.push(
 		// {
		// 	title: 'Rentee',
		// 	dataIndex: 'rentee'
		// },
		// {
		// 	title: 'Due',
		// 	dataIndex: 'due'
		// },
		// {
		// 	title: 'Damage Fine',
		// 	dataIndex: 'damageFine'
		// },
		{
			title: 'Action',
			dataIndex: '',
			render: (record) => (
				<Space>
					<Button type='primary' ghost onClick={() => showModal(record.carId)}>Edit</Button>
					<Button type="primary" danger ghost onClick={() => deleteCar(record.carId)} style={{ color: 'red' }}>Delete</Button>
				</Space>
			),
		});
	}

	const handleOk = async () => {
		setIsModalVisible(false);

		try {
			const body = {
				model,
				licensePlate,
				pricePerDay
			};

			const requestOptions = {
				// method: 'PATCH',
				body: JSON.stringify(body),
			};

			const response = await patchApi({
				url: 'cars/' + carId,
				options: requestOptions,
			});

			const result = await response;

			message.success('Car updated successfully');
			fetchCars();
		} catch (err) {
			message.error('Error while updating the Car');
			console.log(err);
		}
	};

	const handleCancel = () => {
		setIsModalVisible(false);
	};

	useEffect(() => {
		fetchCars();
	}, [toggleCar]);

	return (
		<>
			<Card title="Car Informations" extra={<Button type="primary" ghost onClick={fetchCars}>Refresh</Button>}>
				<Table columns={columns} dataSource={data} />
			</Card>

			<Modal
				title="Edit Car Information"
				visible={isModalVisible}
				onOk={handleOk}
				onCancel={handleCancel}
				footer={[
					<Button key="back" onClick={handleCancel}>
						Cancel
					</Button>,
					<Button key="submit" type="primary" onClick={handleOk}>
						Save Changes
					</Button>,
				]}
			>
				<Form
					labelCol={{
						span: 5,
					}}
					wrapperCol={{
						span: 18,
					}}
					layout="horizontal"
					size="default"
				>
					<Form.Item label="Car Id">
						<span className="ant-form-text">{carId}</span>
					</Form.Item>
					<Form.Item label="Model">
						<Input
							style={{ width: '100%' }}
							placeholder="Enter Car's model"
							value={model}
							onChange={(e) => setModel(e.target.value)}
						/>
					</Form.Item>
					<Form.Item label="License Plate">
						<Input
							style={{ width: '100%' }}
							placeholder="Enter Car's license Plate"
							value={licensePlate}
							onChange={(e) => setLicensePlate(e.target.value)}
						/>
					</Form.Item>
					<Form.Item label="pricePerDay">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter Price Per Day"
							value={pricePerDay}
							onChange={(e) => setPricePerDay(e)}
						/>
					</Form.Item>
				</Form>
			</Modal>
		</>

	);
}

CarsTable.propTypes = {
	toggleCar: PropTypes.bool,
};

export default CarsTable;


