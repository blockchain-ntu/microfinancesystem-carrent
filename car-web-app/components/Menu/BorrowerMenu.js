import React from 'react';
import 'antd/dist/antd.css';
import { Menu } from 'antd';

import { useRouter } from 'next/router';

function BorrowerMenu() {
	const router = useRouter();

	return (
		<Menu
			mode="inline"
			defaultSelectedKeys={['/rent']}
			style={{ height: '100%', borderRight: 0 }}
		>
			<Menu.Item key="/rent" onClick={() => router.push('/borrower/rent')}>
				Rent Car
			</Menu.Item>
			<Menu.Item key="/insurance" onClick={() => router.push('/borrower/insurance')}>
				Action Hub
			</Menu.Item>
		</Menu>
	);
}

export default BorrowerMenu;
