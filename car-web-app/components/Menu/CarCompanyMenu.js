import React from 'react';
import 'antd/dist/antd.css';
import { Menu } from 'antd';

import { useRouter } from 'next/router';

function CarCompanyMenu() {
	const router = useRouter();
	const { SubMenu } = Menu;

	return (
		<Menu
			mode="inline"
			defaultSelectedKeys={['/add-cars']}
			style={{ height: '100%', borderRight: 0 }}
		>
			{/* car rental */}
			{/* When clicks the Car Rental Information menu item it will load the component placed inside the 'pages/manager/add-cars' javascript file. */}
			<Menu.Item key="/add-cars" onClick={() => router.push('/car_company/add-cars')}>Add New Car</Menu.Item>
			<Menu.Item key="/rental-request" onClick={() => router.push('/car_company/rental-request')}>Rental Cars Request</Menu.Item>
			<Menu.Item key="/edit-status" onClick={() => router.push('/car_company/edit-status')}>
				Edit Return Car Status
			</Menu.Item>
		</Menu>
			
	);
}
export default CarCompanyMenu;