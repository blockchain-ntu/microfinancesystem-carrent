import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, message, Modal, Form, InputNumber, Input, Space, Button } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import UserContext from '../../stores/userContext';
import SmartContractContext from '../../stores/smartContractContext';

function RentTable() {
	const { user } = useContext(UserContext);
	const { confirm, info } = Modal;
	const { RentalContract, UserIdentityContract, MicroTokenContract } = useContext(SmartContractContext);

	const state = [
		"REQUESTED",
		"INSURANCE_APPLIED",
		"INSURANCE_APPROVED",		
		"INSURANCE_REJECTED",
		"CAROWNER_APPROVED",
		"CAROWNER_REJECTED",
		"PAID_TO_INSURANCE",
		"PAID_TO_CAROWNER",
		"FINE_REQUESTED",
		"FINE_PAID",
		"CLOSE",
		"CLAIM_REQUESTED",
		"CLAIMED",
		"DEFAULT"
	];

	const [isInsuranceModalVisible, setIsInsuranceModalVisible] = useState(false);
	const [id, setId] = useState(-1);
	const [insuranceId, setInsuranceId] = useState('');
	const [insurance, setInsurance] = useState('');
	const [insuranceFee, setInsuranceFee] = useState('');

	const [data, setData] = useState([]);

	const [payments, setPayments] = useState([]);
	const [rentRecord, setRentRecord] = useState({});
	const [tokenTransferStep, setTokenTransferStep] = useState(0);
	const [isInsuranceTransferModalVisible, setIsInsuranceTransferModalVisible] = useState(false);
	const [isCarTransferModalVisible, setIsCarTransferModalVisible] = useState(false);
	const [isCarFineTransferModalVisible, setIsCarFineTransferModalVisible] = useState(false);
	const [isBorrowerTransferModalVisible, setIsBorrowerTransferModalVisible] = useState(false);

	const borrowers = {};
	const insurers = {};


	const getBorrowers = async () => {
		const response = await UserIdentityContract.methods.getAllBorrowers().call();
		for (let i = 0; i < response.length; i++) {
			borrowers[response[i].walletAddress] = response[i].name;
		}
	};

	const getInsuranceCompanies = async () => {
		const response = await UserIdentityContract.methods.getAllInsurers().call();
		for (let i = 0; i < response.length; i++) {
			insurers[response[i].walletAddress] = response[i].name;
		}
	};

	const getCarOwner = async () => {
		const response = await RentalContract.methods.getCarOwner().call();
	}

	const getPayments = async () => {
		try {
			const response = await getApi({
				url: 'rent-payments',
			});
			const paymentsResult = await response;
			setPayments(paymentsResult);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Rent Payments');
		}
	};


	const getRentals = async () => {
		try {
			const response = await RentalContract.methods.getRentals().call();
			console.log(response)
			setData([]);
	
			if (response.length > 0) {
				for (let i = 0; i < response.length; i++) {
					const row = {
						key: response[i].id,
						id: response[i].id,
						rentee: response[i].rentee,
						carId: response[i].carId,
						totalDays: response[i].totalDays,
						due: response[i].due,
						insurance: response[i].insurance,
						insuranceFee: response[i].insuranceFee,
						damageFine: response[i].damageFine,
						InsurancePolicyId: response[i].InsurancePolicyId,
						status: response[i].state,
					};
					setData((prev) => {
						return [...prev, row];
					});
				}
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading car-rental requests');
		}
	};

	const loadData = async () => {
		await getBorrowers();
		await getInsuranceCompanies();
		//await getPayments();
		await getRentals();
	};

	const approveRental = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.rentApproved(id).send({ from: accounts[0] });
			message.success(`Rental ${id} approved`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while approving the Rental');
		}
	};

	const confirmRentalApprove = (id) => {
		confirm({
			content: `Approve Rental ${id} ?`,
			okText: 'Approve Rental',
			onOk: () => approveRental(id),
		});
	};

	const requestClaim = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.requestClaim(id).send({ from: accounts[0] });
			message.success(`Rental ${id} claim requested`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while requesting claim');
		}
	};

	const confirmReceivingOfClaim = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.confirmReceivingOfClaim(id).send({ from: accounts[0] });
			message.success(`Rental ${id} Claim has been received`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while declaring claim has been received.');
		}
	};

	const approveInsuranceApproved = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.insuranceApproved(id).send({ from: accounts[0] });
			message.success(`Rental ${id} Insurance approved`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while approving the insurance for Rental');
		}
	};

	const insuranceApproved = (id) => {
		confirm({
			content: `Insurance approved for Rental ${id} ?`,
			okText: 'Insurance Approved',
			onOk: () => approveInsuranceApproved(id),
		});
	};

	const approveInsuranceRejected = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.insuranceRejected(id).send({ from: accounts[0] });
			message.success(`Rental ${id} Insurance Rejected`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while rejecting the insurance for Rental');
		}
	};

	const insuranceRejected = (id) => {
		confirm({
			content: `Insurance reject for Rental ${id} ?`,
			okText: 'Insurance Rejected',
			onOk: () => approveInsuranceRejected(id),
		});
	};

	const approveMarkAsDefault = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.markAsDefault(id).send({ from: accounts[0] });
			message.success(`Car has been resetted for Rental ${id} successfully`);
			loadData();
			await getRentals();
		} catch (err) {
			console.log(err);
			message.error('Error occured while resetting car.');
		}
	};

	const markAsDefault = (id) => {
		confirm({
			content: `Rental ${id} mark back to default?`,
			okText: 'Rental has been resetted.',
			onOk: () => approveMarkAsDefault(id),
		});
	};

	const approveAdminReceiveBackCar = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.adminReceiveBackCar(id).send({ from: accounts[0] });
			message.success(`Car has been returned with no damage for Rental ${id} successfully`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while returning car.');
		}
	};

	const adminReceiveBackCar = (id) => {
		confirm({
			content: `Car has been returned with no damage for Id: Rental ${id} ?`,
			okText: 'Returned successfully',
			onOk: () => approveAdminReceiveBackCar(id),
		});
	};

	const approveRequestFine = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.requestFine(id).send({ from: accounts[0] });
			message.success(`Fine Requested for Rental ${id} successfully`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while requesting for fine.');
		}
	};

	const requestFine = (id) => {
		confirm({
			content: `Request fine for Rental ${id} ?`,
			okText: 'Fine Requested Successfully',
			onOk: () => approveRequestFine(id),
		});
	};

	const rejectRental = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			console.log(accounts[0])
			await RentalContract.methods.rentRejected(id).send({ from: accounts[0] });
			console.log("success")
			message.success(`Rental ${id} rejected`);
			loadData();
		} catch (err) {
			message.error('Error occured while rejecting the Rental');
		}
	};

	const confirmRentalReject = (id) => {
		confirm({
			icon: <CloseCircleOutlined style={{ color: 'red' }} />,
			content: `Reject Rental ${id} ?`,
			okText: 'Reject Rental',
			okButtonProps: {
				type: 'danger',
			},
			onOk: () => rejectRental(id),
		});
	};

	const confirmTokenTransferToInsurance = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.confirmTokenTransferToInsurance(id).send({ from: accounts[0] });
			message.success(`Rent ${id} updated`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Rent');
		}
	};

	const confirmTokenTransferToCarCompany = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.confirmTokenTransferToCarCompany(id).send({ from: accounts[0] });
			message.success(`Rent ${id} updated`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Rent');
		}
	};

	const confirmRecivingOfFine = async (id) => {
        try {
            const accounts = await window.ethereum.enable();
            await RentalContract.methods.confirmRecivingOfFine(id).send({ from: accounts[0] });
            message.success(`Rent ${id} updated`);
            loadData();
        } catch (err) {
            console.log(err);
            message.error('Error occured while updating Rent');
        }
    }; 

	const transferTokensToInsurance = async () => {
		try {
			const accounts = await window.ethereum.enable();
			const response = await MicroTokenContract.methods.transfer(rentRecord.insurance, rentRecord.insuranceFee).send({
				from: accounts[0] });
			// setTransactionHash(response.transactionHash);
			message.success('Token transferred successfully');
			setTokenTransferStep(1);
			await confirmTokenTransferToInsurance(rentRecord.id);
			setTokenTransferStep(0);
			setIsInsuranceTransferModalVisible(false);
			showTransactionHash(response.transactionHash);
		} catch (err) {
			console.log(err);
			await setTokenTransferStep(0);
		}
	};


	const transferTokensToCar = async () => {
		try {
			const accounts = await window.ethereum.enable();
			const response = await MicroTokenContract.methods.transfer("0xEb65fcA9fa6AB1D9A3FC37EE9b30585cCD985A93", rentRecord.due).send({
				from: accounts[0] });
			// setTransactionHash(response.transactionHash);
			message.success('Token transferred successfully');
			await setTokenTransferStep(1);
			await confirmTokenTransferToCarCompany(rentRecord.id);
			await setTokenTransferStep(0);
			setIsCarTransferModalVisible(false);
			showTransactionHash(response.transactionHash);
		} catch (err) {
			console.log(err);
			await setTokenTransferStep(0);
		}
	};

	const transferTokensToCarFine = async () => {
		try {
			const accounts = await window.ethereum.enable();
			const response = await MicroTokenContract.methods.transfer("0xEb65fcA9fa6AB1D9A3FC37EE9b30585cCD985A93", "20").send({
				from: accounts[0] });
			// setTransactionHash(response.transactionHash);
			message.success('Token transferred successfully');
			await setTokenTransferStep(1);
			await confirmRecivingOfFine(rentRecord.id);
			await setTokenTransferStep(0);
			setIsCarFineTransferModalVisible(false);
			showTransactionHash(response.transactionHash);
		} catch (err) {
			console.log(err);
			await setTokenTransferStep(0);
		}
	};

	const addInsuranceToRent = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.addInsurance(id, insurance, insuranceFee, insuranceId).send({ from: accounts[0] });
			message.success(`Insurance added successfully to the Rent ${id}`);
			loadData();
		} catch (err) {
			message.error('Error occured while adding insurance');
		}
	};


	const showBorrowerTransferModal = (row) => {
		setRentRecord(row);
		setIsBorrowerTransferModalVisible(true);
	};

	const showInsuranceTransferModal = (row) => {
		setRentRecord(row);
		setIsInsuranceTransferModalVisible(true);
	};

	const showCarTransferModal = (row) => {
		setRentRecord(row);
		setIsCarTransferModalVisible(true);
	};

	const showCarFineTransferModal = (row) => {
		setRentRecord(row);
		setIsCarFineTransferModalVisible(true);
	};

	const showInsuranceModal = (value) => {
		setId(value);
		setIsInsuranceModalVisible(true);
	};

	const handleInsurance = () => {
		addInsuranceToRent();
		setIsInsuranceModalVisible(false);
	};

	const handleCancel = () => {
		setIsInsuranceModalVisible(false);
		setIsInsuranceTransferModalVisible(false);
		setIsBorrowerTransferModalVisible(false);
		setIsCarTransferModalVisible(false);
		setIsCarFineTransferModalVisible(false);
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			render: text => text,
		},
		{
			title: 'Borrower Address',
			dataIndex: 'rentee',
		},
		{
			title: 'Total Days',
			dataIndex: 'totalDays',
		},
		{
			title: 'Due',
			dataIndex: 'due',
		},
		{
			title: 'Status',
			dataIndex: 'status',
			render: tag => {
				let color = 'geekblue';
				if (tag === '4' || tag === '6' || tag === '10') {
					color = 'red';
				} else if (tag === '5' || tag === '9') {
					color = 'green';
				}
				return (
					<Tag color={color} key={tag}>
						{state[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'car_company') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '2') {
					actionBlock =
						<Space>
							<Button type="primary" ghost onClick={() => confirmRentalApprove(record.id)}> Approve </Button>
							<Button type="primary" danger ghost onClick={() => confirmRentalReject(record.id)}> Reject </Button>
						</Space>;
				} 
				else if (record.status === '7'){
					actionBlock = 
						<Space>
							<Button type="primary" ghost onClick={() => requestFine(record.id)}> Request Fine </Button>
							<Button type="primary" danger ghost onClick={() => adminReceiveBackCar(record.id)}> No Damages </Button>
						</Space>;
				} else if (record.status === '9'){
					actionBlock = 
						<Space>
							<Button type="primary" ghost onClick={() => adminReceiveBackCar(record.id)}> Fine Received </Button>
						</Space>;
				} else if (record.status === '10'){
					actionBlock = 
						<Space>
							<Button type="primary" ghost onClick={() => markAsDefault(record.id)}> Return Car Back to Company </Button>
						</Space>;
				} else if (record.status === '12'){
					actionBlock = 
						<Space>
							<Button type="primary" ghost onClick={() => markAsDefault(record.id)}> Return Car Back to Company </Button>
						</Space>;
				}
				return actionBlock;
			},
		});
	} else if (user.role === 'borrower') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '0') {
					actionBlock =
						<Button type="primary" ghost onClick={() => showInsuranceModal(record.id)}>
							Add Insurance
						</Button>;
				} else if (record.status === '1') {
					actionBlock =
					<Space>
						<Button type="primary" ghost onClick={() => insuranceApproved(record.id)}> Approve </Button>
						<Button type="primary" danger ghost onClick={() => insuranceRejected(record.id)}> Reject </Button>
					</Space>;
				} else if (record.status === '4') {
					//paid to insurance
					actionBlock =
						<Button type="primary" ghost onClick={() => showInsuranceTransferModal(record)}>
							Transfer Tokens to Insurance Co.
						</Button>;
				} else if (record.status === '6') {
					//paid to car owner
					actionBlock =
						<Button type="primary" ghost onClick={() => showCarTransferModal(record)}>
							Pay Car
						</Button>;
				} else if (record.status === '8') {
					// pay fine
					actionBlock =
						<Button type="primary" ghost onClick={() => showCarFineTransferModal(record)}>
							Pay Fine
						</Button>;
				} else if (record.status === '10') {
					actionBlock =
						<Button type="primary" ghost onClick={() => requestClaim(record.id)}>
							Request Claim
						</Button>;					
					// claim request 
				} else if (record.status === '11') {
					// claim request 
					actionBlock =
						<Button type="primary" ghost onClick={() => confirmReceivingOfClaim(record.id)}>
							Confirm Claim
						</Button>;	
				} 

				return actionBlock;
			},

			// "REQUESTED", 0 
			// "INSURANCE_APPLIED", 1
			// "INSURANCE_APPROVED", 2		
			// "INSURANCE_REJECTED", 3
			// "CAROWNER_APPROVED", 4
			// "CAROWNER_REJECTED", 5
			// "PAID_TO_INSURANCE", 6
			// "PAID_TO_CAROWNER", 7
			// "FINE_REQUESTED", 8
			// "FINE_PAID", 9
			// "CLOSE", 10
			// "CLAIM_REQUESTED", 11
			// "CLAIMED", 12
			// "DEFAULT" 13
		});
	}
	useEffect(() => {
		loadData();
	}, []);

	return (
		<>
			<Card title="Rentals" extra={<Button type="primary" ghost onClick={loadData}>Refresh</Button>}>
				<Table
					pagination="true"
					columns={columns}
					dataSource={data}
				/>
			</Card>
			<Modal
				title={`Add Insurance - Rental Request ${id}`}
				visible={isInsuranceModalVisible}
				onOk={handleInsurance}
				onCancel={handleCancel}
				width="50%"
			>
				<Form
					labelCol={{ lg: 6, xl: 6, xxl: 5 }}
					wrapperCol={{ lg: 12, xl: 12, xxl: 16 }}
					layout="horizontal"
					size="default"
					labelAlign="left"
				>
					<Form.Item label="Rental ID">
						<Input
							placeholder="Enter Rental ID"
							value={id}
							disabled="true"
						/>
					</Form.Item>
					<Form.Item label="Insurance Address">
						<Input
							placeholder="Enter Insurance Wallet Address"
							value={insurance}
							onChange={(e) => setInsurance(e.target.value)}
						/>
					</Form.Item>
					<Form.Item label="Insurance Fee">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter Insurance fee pay by Bank"
							value={insuranceFee}
							onChange={(e) => setInsuranceFee(e)}
						/>
					</Form.Item>
					<Form.Item label="Insurance Policy ID">
						<InputNumber
							min="0"
							style={{ width: '100%' }}
							placeholder="Enter Insurance Policy Id"
							value={insuranceId}
							onChange={(e) => setInsuranceId(e)}
						/>
					</Form.Item>
				</Form>
			</Modal>

			<Modal
				title={`Transfer Tokens to Insurance Company - Rent Id ${rentRecord.id}`}
				visible={isInsuranceTransferModalVisible}
				width={700}
				onCancel={handleCancel}
				footer={null}
			>
				{
					tokenTransferStep === 0 &&
					<Form
						labelCol={{ lg: 6, xl: 5, xxl: 6 }}
						wrapperCol={{ lg: 20, xl: 20, xxl: 20 }}
						layout="horizontal"
						size="default"
						labelAlign="left"
					>
						<Form.Item label="Insurance Co. Name" style={{ marginBottom: '0px' }}>
							<span> { "ABC Insurance" } </span>
						</Form.Item>
						<Form.Item label="Insurance Co. Address" style={{ marginBottom: '0px' }}>
							<span> { rentRecord.insurance } </span>
						</Form.Item>
						<Form.Item label="Amount">
							<span> { rentRecord.insuranceFee } </span>
						</Form.Item>
						<Form.Item wrapperCol={{
							lg: { span: 14, offset: 6 },
							xl: { span: 14, offset: 5 },
							xxl: { span: 14, offset: 6 } }}
						>
							<Space direction="horizontal">
								<Button onClick={() => handleCancel()}>Cancel</Button>
								<Button type="primary" onClick={() => transferTokensToInsurance()}>Transfer Tokens</Button>
							</Space>
						</Form.Item>
					</Form>
				}
				{
					tokenTransferStep === 1 &&
					<span>Updating the State</span>
				}
			</Modal>

			<Modal
				title={`Transfer Tokens to Car Company - Rent Id ${rentRecord.id}`}
				visible={isCarTransferModalVisible}
				width={700}
				onCancel={handleCancel}
				footer={null}
			>
				{
					tokenTransferStep === 0 &&
					<Form
						labelCol={{ lg: 6, xl: 5, xxl: 6 }}
						wrapperCol={{ lg: 20, xl: 20, xxl: 20 }}
						layout="horizontal"
						size="default"
						labelAlign="left"
					>
						<Form.Item label="Car Company" style={{ marginBottom: '0px' }}>
							<span> { "Car Company" } </span>
						</Form.Item>
						<Form.Item label="Car Co. Address" style={{ marginBottom: '0px' }}>
							<span> { rentRecord.rentee } </span>
						</Form.Item>
						<Form.Item label="Amount">
							<span> { rentRecord.due } </span>
						</Form.Item>
						<Form.Item wrapperCol={{
							lg: { span: 14, offset: 6 },
							xl: { span: 14, offset: 5 },
							xxl: { span: 14, offset: 6 } }}
						>
							<Space direction="horizontal">
								<Button onClick={() => handleCancel()}>Cancel</Button>
								<Button type="primary" onClick={() => transferTokensToCar()}>Transfer Tokens</Button>
							</Space>
						</Form.Item>
					</Form>
				}
				{
					tokenTransferStep === 1 &&
					<span>Updating the State</span>
				}

			</Modal>

			<Modal
				title={`Transfer Fine Tokens to Car Company - Rent Id ${rentRecord.id}`}
				visible={isCarFineTransferModalVisible}
				width={700}
				onCancel={handleCancel}
				footer={null}
			>
				{
					tokenTransferStep === 0 &&
					<Form
						labelCol={{ lg: 6, xl: 5, xxl: 6 }}
						wrapperCol={{ lg: 20, xl: 20, xxl: 20 }}
						layout="horizontal"
						size="default"
						labelAlign="left"
					>
						<Form.Item label="Car Company" style={{ marginBottom: '0px' }}>
							<span> { "Car Company" } </span>
						</Form.Item>
						<Form.Item label="Car Co. Address" style={{ marginBottom: '0px' }}>
							<span> { "0x767C301fd0CFAce0964daB7260fE9C59D6424eBD" } </span>
						</Form.Item>
						<Form.Item label="Amount">
							<span> { "20" } </span>
						</Form.Item>
						<Form.Item wrapperCol={{
							lg: { span: 14, offset: 6 },
							xl: { span: 14, offset: 5 },
							xxl: { span: 14, offset: 6 } }}
						>
							<Space direction="horizontal">
								<Button onClick={() => handleCancel()}>Cancel</Button>
								<Button type="primary" onClick={() => transferTokensToCarFine()}>Transfer Tokens</Button>
							</Space>
						</Form.Item>
					</Form>
				}
				{
					tokenTransferStep === 1 &&
					<span>Updating the State</span>
				}
			</Modal>
		</>
	);

}

export default RentTable;
