import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, message, Modal, Form, InputNumber, Input, Space, Button } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import UserContext from '../../stores/userContext';
import { getApi } from '../../util/fetchApi';
import SmartContractContext from '../../stores/smartContractContext';

function ReturnCarStatusTable() {
	const { user } = useContext(UserContext);
	const { confirm , info } = Modal;
	const { RentalContract, UserIdentityContract, MicroTokenContract } = useContext(SmartContractContext);

    const state = [
        "REQUESTED",
		"INSURANCE_APPLIED",
		"INSURANCE_APPROVED",
		"INSURANCE_REJECTED",
		"CAROWNER_APPROVED",
		"CAROWNER_REJECTED",
		"PAID_TO_INSURANCE",
		"PAID_TO_CAROWNER",
		"FINE_REQUESTED",
		"FINE_PAID",
		"CLOSE",
		"CLAIM_REQUESTED",
		"CLAIMED",
		"DEFAULT"
    ];

	const [data, setData] = useState([]);
	const [isModalVisible, setIsModalVisible] = useState(false);
	const [carId, setCarId] = useState('');
	//const [carModel, setCarModel] = useState('');
	//const [licensePlate, setLicensePlate] = useState('');
	const [pricePerDay, setPricePerDay] = useState('');
	const [carRecord, setCarRecord] = useState({});
	const [isInsuranceTransferModalVisible, setIsInsuranceTransferModalVisible] = useState(false);
	const [tokenTransferStep, setTokenTransferStep] = useState(0);
	const [isCarCompanyTransferModalVisible, setCarCompanyTransferModalVisible] = useState(false);
	const [payments, setPayments] = useState([]);
	//const [id, setId] = useState(-1);
	

	const getInsuranceCompanies = async () => {
		try {
			const response = await UserIdentityContract.methods.getAllInsurers().call();
			setData([]);
	
				 // Update data array using brokers data returned from User Identity smart contract.
				for (let i = 0; i < response.length; i++) {
					 const row = {
						 key: response[i].id,
						 insuranceCoid: response[i].id,
						 //carOwnerSocialId: response[i].socialSecurityId,
						 insuranceCoAddress: response[i].walletAddress,
						 insuranceCoName: response[i].name,
						 //status: response[i].state,
					};
	
					setData((prev) => {
						return [...prev, row];
					});
				}
		} catch (err) {
				console.log(err);
				message.error('Error occured while loading insurance companies');
		}
	};

	const getCarCompanies = async () => {
		try {
			const response = await UserIdentityContract.methods.getAllCarOwners().call();
			setData([]);
	
				 // Update data array using brokers data returned from User Identity smart contract.
				for (let i = 0; i < response.length; i++) {
					 const row = {
						 key: response[i].id,
						 carOwnerid: response[i].id,
						 //carOwnerSocialId: response[i].socialSecurityId,
						 carOwnerAddress: response[i].walletAddress,
						 carOwnerName: response[i].name,
						 //status: response[i].state,
					};
	
					setData((prev) => {
						return [...prev, row];
					});
				}
		} catch (err) {
				console.log(err);
				message.error('Error occured while loading car companies');
		}
	};

	const getRentals = async () => {
		try {
			const response = await RentalContract.methods.getRentals().call();
			console.log(response)
			
			const cars = await getApi({
				url: 'cars',
			});

			setData([]);
	
			if (response.length > 0) {
				for (let i = 0; i < response.length; i++) {
					for(let j=0; j < cars.length; j++) {

						if(cars[j]._id === response[i].carId) {
							let totalRentalPrice = response[i].totalDays * cars[j].pricePerDay;
						}

						const row = {
							key: response[i].carId,
							carId: response[i].carId,
							id: response[i].id,
							rentee: response[i].rentee,
							totalDays: response[i].totalDays,
							insuranceFee: response[i].insuranceFee,
							damageFine: response[i].damageFine,
							insurancePolicyId: response[i].insurancePolicyId,
							status: response[i].state,
						};

						row.totalRentalPrice = totalRentalPrice

						setData((prev) => {
							return [...prev, row];
						});
					}				
				}
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading car-rental requests');
		}
	};

	const getPayments = async () => {
		try {
			const response = await getApi({
				url: 'rent-payments',
			});
			const paymentsResult = await response;
			setPayments(paymentsResult);
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading Loan Payments');
		}
	};

	const loadData = async () => {
		await getInsuranceCompanies();
		await getCarCompanies();
		await getRentals();
	};

	const confirmTokenTrasferToInsurance = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.confirmTokenTransferToInsurance(id).send({ from: accounts[0] });
			message.success(`Rental ${id} Insurance updated`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Insurance');
		}
	};

	const showTransactionHash = (hash) => {
		info({
			content: `Transaction Hash: ${hash}`,
			width: 500,
		});
	};

	const transferTokensToInsurance = async () => {
		try {
			const accounts = await window.ethereum.enable();
			const response = await MicroTokenContract.methods.transfer(carRecord.insuranceCoAddress, carRecord.insuranceFee).send({
				from: accounts[0] });
			// setTransactionHash(response.transactionHash);
			message.success('Token transferred successfully');
			setTokenTransferStep(1);
			await confirmTokenTrasferToInsurance(carRecord.id);
			setTokenTransferStep(0);
			setIsInsuranceTransferModalVisible(false);
			//showTransactionHash(response.transactionHash);
		} catch (err) {
			console.log(err);
			await setTokenTransferStep(0);
			message.error('Error occured while transferring tokens');
		}
	};

	const confirmTokenTrasferToCarCompany = async (id) => {
		try {
			const accounts = await window.ethereum.enable();
			await RentalContract.methods.confirmTokenTransferToCarCompany(id).send({ from: accounts[0] });
			message.success(`Car ${id} Rental Payment updated`);
			loadData();
		} catch (err) {
			console.log(err);
			message.error('Error occured while updating Rental Payment');
		}
	};

	const transferTokensToCarCompany = async () => {
		try {
			const accounts = await window.ethereum.enable();
			await MicroTokenContract.methods.transfer(carRecord.carOwnerAddress, carRecord.totalRentalPrice).send({
				from: accounts[0] });
			message.success('Token transferred successfully');
			await setTokenTransferStep(1);
			await confirmTokenTrasferToCarCompany(carRecord.id);
			await setTokenTransferStep(0);
			await setIsCarCompanyTransferModalVisible(false);
			showTransactionHash(response.transactionHash);
		} catch (err) {
			console.log(err);
			await setTokenTransferStep(0);
			message.error('Error occured while transferring tokens');
		}
	};


	const showInsuranceTransferModal = (row) => {
		setCarRecord(row);
		setIsInsuranceTransferModalVisible(true);
	};

	const showCarCompanyTransferModal = (row) => {
		setCarRecord(row);
		setIsCarCompanyTransferModalVisible(true);
	};

	const handleCancel = () => {
		setIsInsuranceTransferModalVisible(false);
		setIsCarCompanyTransferModalVisible(false);
	};


	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			render: text => text,
		},
		{
			title: 'Borrower Address',
			dataIndex: 'rentee',
		},
		{
			title: 'Total Rental Price',
			dataIndex: 'totalRentalPrice',
		},
		{
			title: 'Status',
			dataIndex: 'status',
			render: tag => {
				let color = 'geekblue';
				if (tag === '13') {
					color = 'red';
				} else if (tag === '4') {
					color = 'green';
				}
				return (
					<Tag color={color} key={tag}>
						{state[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'borrower') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			render: (record) => {
				let actionBlock = '';
				if (record.status === '4') {
					actionBlock =
					<Button type="primary" ghost onClick={() => showInsuranceTransferModal(record)}>
						Transfer Tokens to Insurance Co.
					</Button>;	
                } else if (record.status === '6') {
					actionBlock =
						<Button type="primary" ghost onClick={() => showCarCompanyTransferModal(record)}>
							Transfer Tokens to Car Company
						</Button>;
				}
				return actionBlock;
			},
		});
	}

	useEffect(() => {
		loadData();
    }, []);

	const expandedRowRender = (record) => {
		const expandedData = [];
		expandedData.push(record);
		const expandedPayments = payments.filter(item => item.rentId == record.id);

		const expandedPaymentColumns = [
			{ title: 'Rent ID', dataIndex: 'rentId', key: 'rentId' },
			{ title: 'Amount', dataIndex: 'amount', key: 'amount' },
			{ title: 'Transaction Hash', dataIndex: 'transactionHash', key: 'transactionHash' },
		];

		return (
			<>
				<Form
					labelCol={{
						lg: 6,
						xl: 6,
						xxl: 3,
					}}
					wrapperCol={{
						lg: 12,
						xl: 12,
						xxl: 16,
					}}
					layout="horizontal"
					size="default"
					labelAlign="left"
				>
					<Form.Item label="Borrower Address" style={{ marginBottom: '0px' }}>
						<span>{record.rentee}</span>
					</Form.Item>
					<Form.Item label="Insurance Name" style={{ marginBottom: '0px' }}>
						<span>{record.insuranceCoName}</span>
					</Form.Item>
					<Form.Item label="Insurance Address" style={{ marginBottom: '0px' }}>
						<span>{record.insuranceCoAddress}</span>
					</Form.Item>
					<Form.Item label="Insurance Fee">
						<span>{record.insuranceFee}</span>
					</Form.Item>
					<Form.Item label="Car Company Name" style={{ marginBottom: '0px' }}>
						<span>{record.carOwnerAddress}</span>
					</Form.Item>
					<Form.Item label="Car Company Address" style={{ marginBottom: '0px' }}>
						<span>{record.carOwnerName}</span>
					</Form.Item>
					<Form.Item label="Total Rental Price">
						<span>{record.totalRentalPrice}</span>
					</Form.Item>
				</Form>
				<Table
					columns={expandedPaymentColumns}
					dataSource={expandedPayments}
					pagination={false}
				/>
			</>
		);
	};


    return (
		<>
			<Card title="Rentals" 
				extra={<Button type="primary" ghost onClick={loadData}>Refresh</Button>}>
				<Table
					pagination="true"
					columns={columns}
					dataSource={data}
					expandable={{
						expandedRowRender,
					}}
				/>
			</Card>
			<Modal
				title={`Transfer Tokens to Insurance Company - Car Id ${carRecord.id}`}
				visible={isInsuranceTransferModalVisible}
				width={700}
				onCancel={handleCancel}
				footer={null}
			>
				{
					tokenTransferStep === 0 &&
					<Form
						labelCol={{ lg: 6, xl: 5, xxl: 6 }}
						wrapperCol={{ lg: 20, xl: 20, xxl: 20 }}
						layout="horizontal"
						size="default"
						labelAlign="left"
					>
						<Form.Item label="Insurance Co. Name" style={{ marginBottom: '0px' }}>
							<span> { carRecord.insuranceCoName } </span>
						</Form.Item>
						<Form.Item label="Insurance Co. Address" style={{ marginBottom: '0px' }}>
							<span> { carRecord.insuranceCoAddress } </span>
						</Form.Item>
						<Form.Item label="Amount">
							<span> { carRecord.insuranceFee } </span>
						</Form.Item>
						<Form.Item wrapperCol={{
							lg: { span: 14, offset: 6 },
							xl: { span: 14, offset: 5 },
							xxl: { span: 14, offset: 6 } }}
						>
							<Space direction="horizontal">
								<Button onClick={() => handleCancel()}>Cancel</Button>
								<Button type="primary" onClick={() => transferTokensToInsurance()}>Transfer Tokens</Button>
							</Space>
						</Form.Item>
					</Form>
				}
				{
					tokenTransferStep === 1 &&
					<span>Updating Car Rental State</span>
				}
			</Modal>
			<Modal
				title={`Transfer Tokens to Car Company - Car Id ${carRecord.id}`}
				visible={isCarCompanyTransferModalVisible}
				width={700}
				onCancel={handleCancel}
				footer={null}
			>
				{
					tokenTransferStep === 0 &&
					<Form
						labelCol={{
							lg: 6,
							xl: 5,
							xxl: 5,
						}}
						wrapperCol={{
							lg: 20,
							xl: 20,
							xxl: 20,
						}}
						layout="horizontal"
						size="default"
						labelAlign="left"
					>
						<Form.Item label="Car Company Name" style={{ marginBottom: '0px' }}>
							<span> { carRecord.carOwnerName } </span>
						</Form.Item>
						<Form.Item label="Car Company Address" style={{ marginBottom: '0px' }}>
							<span> { carRecord.carOwnerAddress } </span>
						</Form.Item>
						<Form.Item label="Amount">
							<span> { carRecord.totalRentalPrice } </span>
						</Form.Item>
						<Form.Item wrapperCol={{
							lg: { span: 14, offset: 6 },
							xl: { span: 14, offset: 5 },
							xxl: { span: 14, offset: 5 } }}
						>
							<Space direction="horizontal">
								<Button onClick={() => handleCancel()}>Cancel</Button>
								<Button type="primary" onClick={() => transferTokensToCarCompany()}>Transfer Tokens</Button>
							</Space>
						</Form.Item>
					</Form>
				}
				{
					tokenTransferStep === 1 &&
					<span>Updating Car Rental State</span>
				}
			</Modal>
		</>
	);
						
}

export default ReturnCarStatusTable;

