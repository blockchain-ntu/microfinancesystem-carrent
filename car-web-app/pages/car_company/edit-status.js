import React from 'react';
import { Row, Col } from 'antd';
import ReturnCarsTable from '../../components/carManagement/ReturnCarsTable';

// React functional component to display the Return Car Status.
function EditReturnCarStatus() {
	return (
		<Row gutter={[16, 16]}>
			<Col span={24}>
				<ReturnCarsTable />
			</Col>
		</Row>
	);
}

export default EditReturnCarStatus;
