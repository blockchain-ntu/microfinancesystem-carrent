import React from 'react';
import { Row, Col } from 'antd';
import RentTable from '../../components/rent/RentTable';

// React functional component display the cars table.
function RentalRequest() {
	return (
		<Row gutter={[16, 16]}>
			<Col span={24}>
				<RentTable />
			</Col>
		</Row>
	);
}

export default RentalRequest;
