import React, { useState } from 'react';
import { Row, Col } from 'antd';
import CarRegistrationForm from '../../components/carManagement/CarRegistrationForm';
import CarsTable from '../../components/carManagement/CarsTable';

// React functional component to display the Car Registration form.
function AddCar() {
	const [toggleCar, setToggleCar] = useState(true);
	return (
		<Row gutter={[16, 16]}>
			<Col span={24}>
				<CarRegistrationForm setToggleCar={setToggleCar} toggleCar={toggleCar}/>
			</Col>
			<Col span={24}>
				<CarsTable toggleCar={toggleCar} />
			</Col>
		</Row>
	);
}

export default AddCar;
