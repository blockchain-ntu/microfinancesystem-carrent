// SPDX-License-Identifier: GPL-3.0
pragma solidity >= 0.7.0 < 0.9.0;

import "./UserIdentity.sol";


contract Rental {
 
    //Different States of each Rental
   enum State {
    REQUESTED,
    INSURANCE_APPLIED,
    INSURANCE_APPROVED, 
    INSURANCE_REJECTED,
    CAROWNER_APPROVED,
    CAROWNER_REJECTED,
    PAID_TO_INSURANCE, 
    PAID_TO_CAROWNER,
    FINE_REQUESTED,
    FINE_PAID,
    CLOSE,
    CLAIM_REQUESTED, 
    CLAIMED,
    DEFAULT
   }

   struct Car {
    bool isAvailable;  // if true, this car can be rented out
    address rentee; // person delegated to
    string carId; // car primary key
    uint id; // First car is 1, then 2 .. 
    State state; // rental state of the car, 'Listed' when newly added
    uint totalDays;
    uint due; // rent fee, 0 when newly added
    uint insuranceFee;
    uint damageFine; // fine need to pay if car damaged, 0 when newly added
    address insurance; 
    uint insurancePolicyId;
   }


    event CarRequest (
    bool isAvailable,
    address rentee,
    string carId,
    uint id,
    State state,
    uint totalDays,
    uint due,
    uint insuranceFee,
    uint damageFine,
    address insurance,
    uint insurancePolicyId
   );

   address private manager;
   UserIdentity identitySC;
   Car[] private rentals;

 
    // The constructor. We assign the manager to be the creator of the contract.
   constructor(address _identitySC){
       manager = msg.sender;
       identitySC = UserIdentity(_identitySC);
   }
 
   //Check if sender isAdmin, this function alone could be added to multiple functions for manager only method calls
   modifier isAdmin() {
       require(msg.sender == manager);
       _;
   }

    modifier isBorrower(address _address)
    {
        require(identitySC.verifyIsBorrower(_address), 'Borrower Only');
        _;
    }

    modifier isCarOwner(address _address)
    {
        require(identitySC.verifyIsCarOwner(_address), 'CarOwner Only');
        _;
    }

    modifier isBank(address _address)
    {
        require(identitySC.verifyIsBank(_address), 'Bank Only');
        _;
    }

    modifier isInsurance(address _address)
    {
        require(identitySC.verifyIsInsurer(_address), 'Invalid Insurance Company');
        _;
    }

    //Check if id is valid
    modifier isValidId(uint _id){
        bool isValid = false;
        for(uint i=0; i< rentals.length; i++)
        {
            if(rentals[i].id == _id)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }
 
    //Check the state it is in
    modifier isIn (uint _id, State _state){
        bool isValid = false;
        for(uint i=0; i< rentals.length; i++)
        {
            if(rentals[i].id == _id && rentals[i].state == _state)
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }

    //Check if state is in either one of the states
    modifier isInEither (uint _id, State _state1, State _state2){
        bool isValid = false;
        for(uint i=0; i< rentals.length; i++)
        {
            if(rentals[i].id == _id && 
                (rentals[i].state == _state1 || rentals[i].state == _state2))
            {
                isValid = true;
                break;
            }
        }
        require(isValid);
        _;
    }

   //Return Total Number of Cars
   function getRentCount() public view returns(uint) {
       return rentals.length;
   }

   function getRentals() public view returns(Car [] memory)
    {
        return rentals;
    }  

    function getCarOwner() public view returns(address){
        return manager;
    }
 
   //Rental Car Enquiry
   function getRentalCarInfo(uint _id) public view returns (Car memory rental) {
       for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                return (rentals[i]);
            }
        }
   }

    //Renting a car -- Borrower Request
    function rentRequest(address _rentee, 
                        string memory _carId, 
                        uint _totalDays) public isBorrower(msg.sender){

        Car memory c = Car (false, 
                        _rentee, 
                        _carId, 
                        rentals.length + 1, 
                        State.REQUESTED, 
                        _totalDays, 
                        _totalDays * 3, 
                        0,
                        0,
                        0x0000000000000000000000000000000000000000,
                        0);
        
        rentals.push(c);

        emit CarRequest(c.isAvailable, 
                        c.rentee, 
                        c.carId, 
                        c.id, 
                        c.state, 
                        c.totalDays, 
                        c.due, 
                        c.insuranceFee,
                        c.damageFine,
                        c.insurance,
                        c.insurancePolicyId);
    }

    //Borrower adds the insurance
    function addInsurance(uint _id, address _insurance, uint _insuranceFee, uint _insurancePolicyId) public 
        isBorrower(msg.sender) 
        isValidId(_id)
        isIn(_id, State.REQUESTED)
        isInsurance(_insurance)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].insurance = _insurance;
                rentals[i].insuranceFee = _insuranceFee;
                rentals[i].insurancePolicyId = _insurancePolicyId;
                rentals[i].state = State.INSURANCE_APPLIED;
                break;
            }
        }
    }

    //Borrower mark Insurance as approve (Once its approved on insurance app)
    function insuranceApproved(uint _id) public 
            isBorrower(msg.sender) 
            isValidId(_id)
            isIn(_id, State.INSURANCE_APPLIED)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.INSURANCE_APPROVED;
                break;
            }
        }
    }
    
    //Borrower mark Insurance as reject 
    function insuranceRejected(uint _id) public 
            isBorrower(msg.sender) 
            isValidId(_id)
            isIn(_id, State.INSURANCE_APPLIED)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.INSURANCE_REJECTED;
                break;
            }
        }
    }

   //Renting a car -- Admin Approve
    function rentApproved(uint _id) public isCarOwner(msg.sender) isValidId(_id) isIn(_id, State.INSURANCE_APPROVED){
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.CAROWNER_APPROVED;
                break;
            }
        }
    }

    //Renting a car -- Admin Reject
    function rentRejected(uint _id) public isCarOwner(msg.sender) isValidId(_id) isIn(_id, State.INSURANCE_APPROVED){
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].isAvailable == true; 
                rentals[i].state = State.CAROWNER_REJECTED;
                break;
            }
        }
    }

    //After renting -- Confirm paid to Insurance
    function confirmTokenTransferToInsurance(uint _id) public 
    isBorrower(msg.sender) isValidId(_id) isIn(_id, State.CAROWNER_APPROVED)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.PAID_TO_INSURANCE;
                break;
            }
        }
    }
    
    //After renting -- Confirm paid to CarCompany
    function confirmTokenTransferToCarCompany(uint _id) public 
    isBorrower(msg.sender) isValidId(_id) isIn(_id, State.PAID_TO_INSURANCE)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.PAID_TO_CAROWNER;
                break;
            }
        }
    }

    //If Fine is Required -- Request Fine
    function requestFine(uint _id) public 
    isCarOwner(msg.sender) isValidId(_id) isIn(_id, State.PAID_TO_CAROWNER)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.FINE_REQUESTED;
                break;
            }
        }
    }

    //After paying Fine
    function confirmRecivingOfFine(uint _id) public 
    isValidId(_id) isIn(_id, State.FINE_REQUESTED)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.FINE_PAID;
                break;
            }
        }
    }


    //After paying Fine or if no Fine is required
    function adminReceiveBackCar(uint _id) public isCarOwner(msg.sender) isValidId(_id) isInEither(_id, State.FINE_PAID, State.PAID_TO_CAROWNER)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.CLOSE;
                break;
            }
        }
    }

    function requestClaim(uint _id) public isBorrower(msg.sender) isValidId(_id) 
                                            isIn(_id, State.CLOSE)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.CLAIM_REQUESTED;
                break;
            }
        }
    }
    
    function confirmReceivingOfClaim(uint _id) public isBorrower(msg.sender) isValidId(_id) 
                                                    isIn(_id, State.CLAIM_REQUESTED)
    {
        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.CLAIMED;
                break;
            }
        }
    }

    function markAsDefault(uint _id) public isCarOwner(msg.sender) 
        isValidId(_id) isInEither(_id, State.CLOSE, State.CLAIMED) {

        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].isAvailable = true;
                rentals[i].rentee = address(0);
                rentals[i].state = State.DEFAULT;
                rentals[i].insurance = address(0);
                rentals[i].insuranceFee = 0;
                rentals[i].insurancePolicyId = 0;
                break;
            }
        }
   }
}

/*
    //Return car (Payment part has been removed following the microfinance structure)
   function returnCarOld(uint _id) public isIn(_id, State.OnLoan) isValidId(_id) isBorrower(rentals[_id].rentee){

       for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].state = State.Return;
                rentals[i].due = 0;
                break;
            }
        }
   }
 
    // Admin check the damage of returned car and set fine amount
    function checkDamage(uint id, bool damage, uint fine) isAdmin isIn(id, State.Return) public
    returns (uint){
        if (damage){
            rentals[id].damageFine = fine ;
            rentals[id].state = State.PayFine; // only damaged car need to pay fine
            return rentals[id].damageFine;
        }
        else{
            rentals[id].state = State.Completed;
            return(0);
            }
    }
 
    // After Rentee pay the fine via UI, admin will trigger this to confirm Payment
    function confirmDamagePaymentReceived(uint _id) public isIn(_id, State.PayFine) isAdmin{

        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
              rentals[i].state = State.Completed;
              break;
            }
        }
    }
 
   // Final function where admin returns the car back to the rental list
   function completeOld(uint _id) isAdmin isIn(_id, State.Completed) public
    returns (bool) {    


        for (uint i = 0; i < rentals.length; i++) {
            if (rentals[i].id == _id) {
                rentals[i].isAvailable = true;
                rentals[i].rentee = address(0);
                rentals[i].damageFine = 0;
                rentals[i].state = State.Listed;
                break;
            }
        }
       return true;
   }

      // Calculating rental price (For enquiry purpose)
   function calculateRentalPrice(uint id, uint totalDays) isValidId(id) public view returns (uint) {
       //Reference to the car that will be rented
       //Car storage carToBeRented = rentals[id];
       return totalDays; // * carToBeRented.pricePerDay;
   }
 
   //Renting a car -- Request
   /*
   function rentRequest(uint _id, uint totalDays) public isValidId(_id) isIn(_id, State.Listed) isBorrower(msg.sender){
       // Reference to the car that will be rented
       Car storage carToBeRented = rentals[_id];
 
      //Retrieve total rental price
       uint totalRental = calculateRentalPrice(_id,totalDays);
 
       require(carToBeRented.isAvailable == true); // Car must be available
 
       //require(totalRental == msg.value); // pay must be equal to rentalPrice
 
       require(totalDays > 0); //Total Days must be more than 0
 
       carToBeRented.rentee = msg.sender; //Assign Rentee to Sender
 
       carToBeRented.isAvailable = false; //Remove Availability (First level of lock)
 
       carToBeRented.due = totalRental; // Assign rent fee
 
       //carToBeRented.state = State.OnLoan; //Rent Success (Second level of lock) //move to rentApproved 

       emit loanRequest(l.id, l.amount, l.months, l.interest, l.planId,
            l.state, l.broker, l.borrower, l.brokerFee, l.insuranceFee, l.insurance, l.insurancePolicyId ); 
   }
   */
