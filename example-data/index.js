const { ethers } = require("ethers");
const UserIdentityBuild = require('./build_contracts/UserIdentity.json')

const provider = new ethers.providers.JsonRpcProvider("http://localhost:7545")

const getAccounts = async () => {
    const accounts = await provider.listAccounts();
    return accounts;
}

const contract = new ethers.Contract(UserIdentityBuild.networks[5777].address, UserIdentityBuild.abi, provider);

const addBorrower = async () => {
    const accounts = await getAccounts();
    const signer = await provider.getSigner(1)
    console.log(await signer.getAddress());
    const contractSigner = await contract.connect(signer);
    contractSigner.addBorrower("3455", accounts[2], 'Rajesh Koothrapali');
}

const addAndApproveBroker = async () => {
    const accounts = await getAccounts();
    const signer = await provider.getSigner(1)
    const contract = new ethers.Contract(UserIdentityBuild.networks[5777].address, UserIdentityBuild.abi, provider);
    const contractSigner = await contract.connect(signer);
    await contractSigner.addBroker("2345", accounts[1], 'Leonard Hofstadter');

    const adminSigner = await provider.getSigner()
    const contractAdminSigner = await contract.connect(adminSigner);
    contractAdminSigner.approveBroker(accounts[1]);
}

const addAndApproveInsurance = async () => {
    const accounts = await getAccounts();
    const signer = await provider.getSigner(3)
    const contract = new ethers.Contract(UserIdentityBuild.networks[5777].address, UserIdentityBuild.abi, provider);
    const contractSigner = await contract.connect(signer);
    await contractSigner.addInsurer("9876", accounts[3], 'ABC Insurance Company');

    const adminSigner = await provider.getSigner()
    const contractAdminSigner = await contract.connect(adminSigner);
    contractAdminSigner.approveInsuranceCompany(accounts[3]);
}

const approveBorrower = async () => {
    const accounts = await getAccounts();
    const signer = await provider.getSigner()
    const contract = new ethers.Contract(UserIdentityBuild.networks[5777].address, UserIdentityBuild.abi, provider);
    const contractSigner = await contract.connect(signer);
    contractSigner.approveBorrower(accounts[2]);
}

const run = async () => {
    await addAndApproveBroker();
    await addBorrower();
    await approveBorrower();
    await addAndApproveInsurance();
}

run();
