Checking Out the GitLab Project
===============================


After successfully install above required dependencies you can clone 
"Microfinance" project from GitLab using following command. ::

   git clone https://gitlab.com/gmc123/microfinance.git

All 5 projects were placed inside the root directory.
We should change our directory to ``microfinance`` directory using following command in the current terminal. ::

   cd microfinance/

This will change your directory to ``microfinance`` directory. 
Check the git branch using the following command. ::

   git branch

Results will indicate that you are in the ``master`` branch. 
This project code is in the ``level2`` branch and we should change our git branch to ``level2`` branch. 
Use the following commands to change the git branch and verify that you are successfully change 
the git branch to ``level2`` branch. ::

   git branch level2
   git branch

In the next section we discuss about the project structure and deployment.
