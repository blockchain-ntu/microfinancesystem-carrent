_app.js
=======

``Next.js`` uses ``App`` Component to initialize pages.
We use it for persist the layout between page changes and keep state when navigating pages.

The default ``_app.js`` when create the ``Next`` app. ::

    // import App from 'next/app'

    function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
    }

    // Only uncomment this method if you have blocking data requirements for
    // every single page in your application. This disables the ability to
    // perform automatic static optimization, causing every page in your app to
    // be server-side rendered.
    //
    // MyApp.getInitialProps = async (appContext) => {
    //   // calls page's `getInitialProps` and fills `appProps.pageProps`
    //   const appProps = await App.getInitialProps(appContext);
    //
    //   return { ...appProps }
    // }

    export default MyApp

You can learn more about Next ``_app.js`` in `Custom App page <https://nextjs.org/docs/advanced-features/custom-app>`_.

As they mention in the above reference:
    - The ``Component`` prop is the active page, 
    - ``pageProps`` is an object with the initial props that were preloaded for the page.

In here ``page`` refer to the scripts in the ``pages`` directory.

    - ``/`` page is refer to ``pages/index.js``
    - ``/bank/loans`` page refer to ``pages/bank/loans.js`` script.

``Component`` prop will automatically loads the ``page`` according to the router.

We override this ``_app.js`` sript as follows. ::

    import React from 'react';

    import BasicLayout from '../components/BasicLayout';
    import { UserContextProvider } from '../stores/userContext';
    import { SmartContractContextProvider } from '../stores/smartContractContext';

    function MyApp({ Component, pageProps }) {
        return (
            <UserContextProvider>
                <SmartContractContextProvider>
                    <BasicLayout>
                        <Component {...pageProps} />
                    </BasicLayout>
                </SmartContractContextProvider>
            </UserContextProvider>
        );
    }

    export default MyApp;

We can wrap the ``Component`` with Layout and Providers.
Layout helps to persist the layout through out the application and Providers help to persist data across the application.

We use both ``UserContextProvider`` and ``SmartContractContextProvider`` here to persist 
user data and smart contract data across the application.

``BasicLayout`` component defines the layout of the application.

You can refer ``_app.js`` example in `Next.js _app.js example <https://jools.dev/nextjs-_appjs-example>`_.