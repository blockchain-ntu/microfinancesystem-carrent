.. warning::
    This is ``level2`` documentation for the Microfinance project. If you are looking for level1 documentation, select the correct branch at the bottom of the left panel.

Application Overview
=====================


.. image:: ../images/tcard_microfinance.png

..

The Microfinance project is about deploying new ERC20 token and use as a alternative currency type. 
We assume that these tokens were use by non-banking people. 
This project has 4 main user roles.

1. ``Bank``: Bank personals who is responsible in ERC20 operations
2. ``Borrower`` : The non-banking people who is getting tokens as loans from the Bank.
3. ``Broker`` : The role who is connecting Bank and Borrowers.
4. ``Insurance Company`` : Insure the token loans.

This project is consists of 5 projects. 

1. Bank web application - ``bank-web-app``
2. Bank serve application - ``bank-server``
3. Insurance Company web application - ``insurance-web-app``
4. Insurance Company server application - ``insurance-server``
5. Blockchain (Truffle project for smart contract development) - ``blockchain``

These projects use different technologies to fullfil the different requirements.
These applications were owned by the Bank and the Insurance Company and used by stakeholders mentioned above.
These project are depending on each other. The following architecture diagram shows the connections between these projects.

Layered Architecture
--------------------

Layered architecture diagram of the system.

.. image:: ../images/layered-architecture.png


In this system we use 4 smart contracts deployed in to the blockchain. 
These smart contracts were accessed by both Bank web application and Insurance Company web application.
The Bank web application communicates with both Bank server and the blockchain.
Like Bank web application, Insurance Company web application communicates with both Insurance Company server 
and the blockchain.

In the following sections we discuss how to run these projects, their dependencies, and functionalities in detail.

Demo Video
----------

.. raw:: html

    <iframe width="356" height="200" src="https://www.youtube.com/embed/SBwFWPUCrmM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

`https://youtu.be/SBwFWPUCrmM <https://youtu.be/SBwFWPUCrmM>`_

