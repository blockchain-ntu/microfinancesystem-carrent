.. Microfinance documentation master file, created by
   sphinx-quickstart on Wed Aug 25 19:56:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. warning::
   The Microfinance project source code and documentation is shared with you for learning purposes only. It should not be commercialized or used for profit purposes.

Welcome to Microfinance dApp documentation
========================================


.. image:: images/tcard_microfinance.png

..

.. note::
   This is ``level2`` documentation for the Microfinance project. If you are looking for level1 documentation, select the correct branch at the bottom of the left panel.

.. toctree::
   :maxdepth: 2
   :caption: Introduction:

   introduction/applicationOverview

.. toctree::
   :maxdepth: 2
   :caption: Installation Guide:

   installation/checkout
   installation/structure
   installation/insuranceServer
   installation/insuranceApp

.. toctree::
   :maxdepth: 3
   :caption: Smart Contracts:

   blockchain/introduction
   blockchain/smartContracts
   blockchain/migration

.. toctree::
   :maxdepth: 2
   :caption: Insurance Co. Web Server

   insuranceServer/introduction
   insuranceServer/index
   insuranceServer/layeredArchitecture

.. toctree::
   :maxdepth: 2
   :caption: Insurance Co. Web Application:

   insuranceClient/introduction
   insuranceClient/userContext
   insuranceClient/smartContractContext
   insuranceClient/app

.. toctree::
   :maxdepth: 2
   :caption: Quickstart:

   quickstart/instructions
