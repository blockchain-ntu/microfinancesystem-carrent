Smart Contracts
===============

In this project we use 5 smart contracts for different requirements. 
More details about these smart contracts are discussed in following sections.
All these smart contracts are resides in the ``contracts`` directory. ::

    Microfinance
        |--blockchain
        |  |--contracts
        |  |  |--BankLoan.sol
        |  |  |--InsurancePolicy.sol
        |  |  |--LoanPayment.sol
        |  |  |--MicroToken.sol
        |  |  |--Migrations.sol
        |  |  |--UserIdentity.sol
        |  |--migrations
        |  |--test
        |  |--truffle-config.js

User Identity Smart Contract - UserIdentity.sol
------------------------------------------------

This contract holds the User details of the Microfinance system. It will register all Broker, Borrower and Insurance Co. details.
In this section we discuss about ``UserIdentity.sol`` in detail.

State Transition Diagram of The User
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following diagram shows the state transition of a User.
We follow this state transition diagram to implement the User state changes in ``UserIdentity`` smart contract.

.. image:: ../images/state_transition_user.png

ENUM
~~~~

1. Role - This enum holds user role values of the system. There 3 user roles in the system. ::

    enum Role { GUEST, BROKER, BORROWER, INSURER }
    
2. UserState - This enum holds the user states. There are 3 states of user in the system. ::

    enum UserState { PENDING, APPROVED, REJECTED }

* ``PENDING`` - Newly registered user. 
* ``APPROVED`` - Bank approved user.
* ``REJECTED`` - Bank rejected user.

Structs
~~~~~~~

1. User - This struct holds the user attributes. ::

    struct Broker{
        uint id; 
        string socialSecurityId; // each Broker has unique social security id
        address walletAddress;
        string name;
        Role role;
        bool isBankApproved;
        UserState state;
    }

2. Borrower - This struct holds the Borrower attributes. ::

    struct Borrower{
        uint id; 
        string socialSecurityId; // each Borrower has unique social security id
        address walletAddress;
        string name;
        Role role;
        address addedBy;
        bool isBankApproved;
        UserState state;
    }

3. InsuranceCompany - This struct holds the Insurance Company attributes. ::

    struct InsuranceCompany{
        uint id; 
        string registrationNumber; // each company has unique registration number
        address walletAddress;
        string name;
        Role role;
        bool isBankApproved;
        UserState state;
    }

* ``id`` - System assigned id number for the user. This is an incremental number.
* ``socialSecurityId`` - This attribute holds the social security.
* ``registrationNumber`` - This attribute holds the insurance comapany's registration number.
* ``walletAddress`` - User account address. Acount address from Ganache.
* ``name`` - User name / Insurance Company name
* ``role`` - User role(BROKER, BORROWER, INSURER)
* ``addedBy`` - The Broker address of who registered the Borrower.
* ``isBankApproved`` - Bank approval.
* ``state`` - State of the user.

Modifiers
~~~~~~~~~

The following modifiers are used in the ``UserIdentity.sol`` functions.

* ``isOwner`` - Checks the function caller is the admin of the contract.
* ``isBroker`` - Checks the function caller is registered as a Broker.
* ``isNewBroker(address _broker)`` - Checks the ``_broker`` address not registered as a Broker in the UserIdentity contract.
* ``isNewIsnsurer(address _insurer)`` - Checks the ``_insurer`` address not registered as a Insurance Company in the UserIdentity contract..
* ``isNewBorrower(address _borrower)`` - Checks the ``_borrower`` address not registered as a Borrower in the UserIdentity contract.
* ``isBrokerIn(address _address, UserState _state)`` - Checks Broker registered in ``_address`` is in the state given by ``_state``.
* ``isBorrowerIn(address _address, UserState _state)`` - Checks Borrower registered in ``_address`` is in the state given by ``_state``.
* ``isInsuranceCompanyIn(address _address, UserState _state)`` - Checks Insurance Company registered in ``_address`` is in the state given by ``_state``.

Attributes
~~~~~~~~~~

``UserIdentity.sol`` contains the following attributes. 

* ``owner`` - Holds the deployer account address of the smart contract. 
* ``brokersCount`` - Holds the total brokers in the system. 
* ``borrowersCount`` - Holds the total borrowers in the system.
* ``insurersCount`` - Holds the total Insurers in the system.
    
* ``borrowers`` - This mapping holds all the borrowers details in the system. (address to Borrower mapping)
* ``brokers`` - This mapping holds all the brokers details in the system. (address to User mapping)
* ``insurers`` - This mapping holds all the Insurers details in the system. (address to User mapping)
    
* ``brokersAddresses`` - This array contains all the brokers addresses.
* ``borrowersAddresses`` - This array contains all the borrowers addresses.
* ``insurersAddresses`` - This array contains all the insurers addresses.

Constructor
~~~~~~~~~~~

The constructor will assign the owner address as the contract deployer(``msg.sender``) address. ::

    constructor()
    {
        owner = msg.sender;
    }


Functions
~~~~~~~~~

addBroker
^^^^^^^^^

This function adds the new broker account to the system. ::

    function addBroker(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isNewBroker(_address)

Parameters:
    * _socialSecurityId - Social Security ID of the Broker.
    * _address - Account address of the Broker.
    * _name - Broker name.
    * _documentsUri - IPFS hash value of the Broker documents.

This function will create a new Broker object with role=BROKER and state=PENDING and map it to the account address.
Before this function executes, ``isNewBroker`` modifier checks this Broker account doesn't exists in the system.

addInsurer
^^^^^^^^^^

This function adds the new insurer account to the system. ::

    function addInsurer(string memory _socialSecurityId, address _address, string memory _name, string memory _documentsUri) public isNewIsnsurer(_address)

Parameters:
    * ``_socialSecurityId`` - Social Security ID of the Insurer.
    * ``_address`` - Account address of the Insurer.
    * ``_name`` - Insurer name.
    * ``_documentsUri`` - IPFS hash value of the Insurer documents.

This function will create a new Insurer object with role=INSURER and state=PENDING and map it to the account address.
Before this function executes, ``isNewInsurer`` modifier checks this Insurer account doesn't exists in the system.

addBorrower
^^^^^^^^^^^

This function adds the new borrower account to the system. ::

    function addBorrower(string memory _socialSecurityId, address _address, string memory name, string memory _documentsUri) public isBroker(msg.sender) isNewBorrower(_address)

Parameters:
    * ``_socialSecurityId`` - Social Security ID of the Borrower.
    * ``_address`` - Account address of the Borrower.
    * ``_name`` - Insurer name.
    * ``_documentsUri`` - IPFS hash value of the Borrower documents.

This function will create a new Borrower object with role=BORROWER and state=PENDING and map it to the account address.
Before this function executes, ``isNewBorrower`` modifier checks this Borrower account doesn't exists in the system.
``isBroker`` will allow only Brokers to add Borrowers into the system.

approveBorrower
^^^^^^^^^^^^^^^^

This function approves the Borrower request created by Broker. ::

    function approveBorrower(address _address) public isOwner(msg.sender)

Parameters:
    * ``_address`` - Address of the Borrower.

This function sets the Borrower attribute isBankApproved to ``true`` and changes the user state to ``APPROVED``.

approveBroker
^^^^^^^^^^^^^

This function approves the Broker registration request. ::

    function approveBroker(address _address) public isOwner(msg.sender)

Parameters:
    * ``_address`` - Address of the Broker.

This function sets the Broker attribute isBankApproved to ``true`` and changes the user state to ``APPROVED``.

approveInsuranceCompany
^^^^^^^^^^^^^^^^^^^^^^^

This function approves the Broker registration request. ::

    function approveInsuranceCompany(address _address) public isOwner(msg.sender)

Parameters:
    * ``_address`` - Address of the Insurer.

This function sets the Insurer attribute isBankApproved to ``true`` and changes the user state to ``APPROVED``.

verifyIsBroker
^^^^^^^^^^^^^^

This function verifies given account address is a Broker account or not. ::

    function verifyIsBroker(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address of the user

This function is used by other smart contracts to verify a Broker account. 
This function will return ``true`` if brokers exists on the given address or ``false`` otherwise.


verifyIsBorrower
^^^^^^^^^^^^^^^^^

This function verifies given account address is a Borrower account or not. ::

    function verifyIsBorrower(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address of the user

This function is used by other smart contracts to verify a Borrower account. 
This function will return ``true`` if Borrower exists on the given address or ``false`` otherwise.

verifyIsInsurer
^^^^^^^^^^^^^^^

This function verifies given account address is a Insurer account or not. ::

    function verifyIsInsurer(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address of the user

This function is used by other smart contracts to verify a Insurer account. 
This function will return ``true`` if Insurer exists on the given address or ``false`` otherwise.

verifyIsBank
^^^^^^^^^^^^

This function verifies given account address is the Bank account or not. ::

    function verifyIsBank(address _address) public view returns(bool)

Parameters:
    * ``_address`` - The account address to be verified.

This function is used by other smart contracts to verify the Bank account. 
This function will return ``true`` if the given address is Bank wallet address; ``false`` otherwise.

getAllBrokers
^^^^^^^^^^^^^

This function returns all the Brokers as an array. ::

    function getAllBrokers() public view returns (User[] memory)

getAllInsurers
^^^^^^^^^^^^^^

This functions returns all the Inurers as an array. ::

    function getAllInsurers() public view returns (User[] memory)

getAllBorrowers
^^^^^^^^^^^^^^^

This functions returns all the Borrowers as an array. ::

    function getAllBorrowers() public view returns (Borrower[] memory)

getBankAddress
^^^^^^^^^^^^^^
This function returns ``UserIdentity`` smart contract ``owner`` address.
The owner of the ``UserIdentity`` smart contract is ``Bank``. ::

    function getBankAddress() public view returns (address _bankAddress)

Events
~~~~~~

These events will emit as follows.

* ``newBrokerAdded`` - This event will emit after successfully create a Broker.
* ``newBorrowerAdded`` - This evevnt will emit after successfully create a Borrower.
* ``newInsurerAdded`` - This event will emit after successfully create a Insurer.

Bank Loan Smart Contract - BankLoan.sol
---------------------------------------

This smart contract stores the Bank Loan details. 
The Bank is the admin of this smart contract.
The following sections describe the components of the smart contract.

State Transition Diagram of The Bank Loan
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following diagram shows the state transition of a Bank Loan.
We follow this state transition diagram to implement the Bank Loan state changes in ``BankLoan`` smart contract.

.. image:: ../images/state_transition_loan.png

ENUM
~~~~

1. LoanState - This enum holds individual loan states. There are 14 loan states. ::

    enum LoanState{
        REQUESTED, 
        INSURANCE_APPLIED, 
        INSURANCE_APPROVED, 
        BORROWER_SIGNED, 
        INSURANCE_REJECTED,
        BANK_APPROVED, 
        BANK_REJECTED,  
        PAID_TO_INSURANCE, 
        PAID_TO_BROKER,
        ONGOING, 
        DEFAULT, 
        CLOSE,
        CLAIM_REQUESTED, 
        CLAIMED
    }

* ``REQUESTED`` - Initial state of a loan. Broker request a loan. 
* ``INSURANCE_APPLIED`` - Broker applied for a insurance. 
* ``INSURANCE_APPROVED`` - Insurance approved by the Insurance Company.
* ``BORROWER_SIGNED`` -Borrower agreed for the Loan. 
* ``INSURANCE_REJECTED`` - Insurance Company rejected the insurance request for the loan.
* ``BANK_APPROVED`` - Bank approved the Loan
* ``BANK_REJECTED`` - Bank rejected the Loan
* ``PAID_TO_BROKER`` - Bank paid the Broker fee. 
* ``PAID_TO_INSURANCE`` - Bank paid the initial insurance fee. 
* ``ONGOING`` - Bank transfer tokens to the Borrower's account. 
* ``DEFAULT`` - Borrower unable to pay back the Loan. 
* ``CLOSE`` - Borrower paid back the Loan.
* ``CLAIM_REQUESTED`` - Bank requested claim for the defaulted loan.
* ``CLAIMED`` - Insurance Company approved the claim request and transfer tokens to the Bank.


Structs
~~~~~~~

1. Loan - This struct holds the Loan attributes. ::

    struct Loan
    {
        uint id;
        uint amount;
        uint months;
        uint interest;
        string planId;
        LoanState state;
        address broker;
        address borrower;
        uint brokerFee;
        uint insuranceFee;
        address insurance;
        uint insurancePolicyId;
    }

* ``id`` - Loan Id
* ``amount`` - Loan amount
* ``months`` - Loan duration in months.
* ``interest`` - Loan interest
* ``planId`` - Loan plan Id
* ``state`` - Current state of the loan
* ``broker`` - Address of the Broker who applied the Loan.
* ``borrower`` - Address of the Borrower of the Loan
* ``brokerFee`` - Broker fee for the Loan.
* ``insuranceFee`` - Initial insurance fee thet should be paid by the Bank for the Loan.
* ``insurance`` - Insurance Company address.
* ``insurancePolicyId`` - Insurance Policy Id

Events
~~~~~~

These events were defined in the ``BankLoan`` smart contract.

loanRequest
^^^^^^^^^^^

This event will emit when Broker create a loan request. ::

    event loanRequest(
        uint id,
        uint amount,
        uint months,
        uint interest,
        string planId,
        LoanState state,
        address broker,
        address borrower,
        uint brokerFee,
        uint insuranceFee,
        address insurance,
        uint insurancePolicyId
    );

Parameters:
    * ``id`` -  Loan Id
    * ``amount`` - Loan amount
    * ``months`` - Duration of the loan
    * ``interest`` - Loan interest
    * ``planId`` - Loan plan id
    * ``state`` - Current state of the loan
    * ``broker`` - Broker of the loan
    * ``borrower`` - Borrower address of the loan
    * ``brokerFee`` - Broker fee of the Loan
    * ``insuranceFee`` - Initial payment amount of the Insurance.
    * ``insurance`` - Isurer address of the loan
    * ``insurancePolicyId`` - Insurance policy id of the loan

Modifiers
~~~~~~~~~

The following modifiers are used in the ``BankLoan.sol`` functions.

* ``isAdmin()`` - Checks the function callers is the owner of the smart contract.
* ``isBroker()`` - Checks the function caller is registered as a Broker in the system.
* ``isInsurance(address _address)`` - Checks the ``_address`` registered as a Insurance Company in UserIdentity contract.
* ``isLoanBroker(_loanId)`` - Checks the function caller is the Broker of the Loan given by ``_loanId``.
* ``isLoanBorrower(uint _loanId)`` - Checks the function callers is the Borrower of the given Loan.
* ``isValidLoan(uint _loanId)`` - Checks Loan exists in the system.
* ``isLoanIn(uint _loanId, LoanState _state)`` - Checks the given Loan is in given Loan State.

Attributes
~~~~~~~~~~

* ``UserIdentity: identitySC`` -  Stores UserIdentity smart contract object
* ``address: admin`` - Store smart contract deployer’s address 
* ``Loan[]: loans`` - Stores loan data

Constructor
~~~~~~~~~~~

The constructor will assign the admin address as the contract deployer(``msg.sender``) address. 
It will require the ``UserIdentity`` smart contract address to deploy the smart contract. 
``UserIdentity`` smart contract address object instance will set as the ``identitySC``. ::

    constructor (address _identitySC) {
            admin = msg.sender;
            identitySC = UserIdentity(_identitySC);
    }


Functions
~~~~~~~~~

applyLoan(...)
^^^^^^^^^^^^^^

Creates a Loan request. ::

    function applyLoan(uint _amount, uint _months, uint _interest, uint _planId, address _borrower) public isBroker()

Parameters: 
    * ``_amount`` - Loan amount
    * ``_months`` - Duration of the loan
    * ``_interest`` - Loan interest
    * ``_planId`` -  Loan plan id
    * ``_borrower`` - Borrower address

Modifiers:
    * ``isBroker`` - Checks the function caller registered as a Broker.

addInsurance(...)
^^^^^^^^^^^^^^^^^

Adds Insurance to the given Loan. ::

    function addInsurance(uint _loanId, address _insurance, uint _insurancePolicyId) public isLoanBroker() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.REQUESTED)

Parameters: 
    * ``_loanId`` - Loan Id
    * `` _insurance`` - Insurance address
    * ``_insurancePolicyId`` - Insurance Policy Id

Modifiers:
    * ``isLoanBroker()`` - The function caller should be the Broker of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.REQUESTED)`` - Checks Loan is in REQESTED state.

Events:
    * ``loanRequest`` - This function will emits loanRequest event.

insuranceApproved(...)
^^^^^^^^^^^^^^^^^^^^^^

Change the Loan state to Insurance approved state. ::

    function insuranceApproved(uint _loanId) public isLoanBroker() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)

Parameters: 
    * ``_loanId`` - Loan Id

Modifiers:
    * ``isLoanBroker()`` - The function caller should be the Broker of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)`` - Checks Loan is in INSURANCE_APPLIED state.

insuranceRejected(...)
^^^^^^^^^^^^^^^^^^^^^^

This function updates the Loan state to Insurance Rejected state. ::

    function isuranceRejected(uint _loanId) public isLoanBroker(_loanId) isValidLoan(_loanId) isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isLoanBroker()`` - The function caller should be the Broker of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.INSURANCE_APPLIED)`` - Checks Loan is in INSURANCE_APPLIED state.

signByBorrower(...)
^^^^^^^^^^^^^^^^^^^

This function is used to sign the Loan by Borrower. ::

    function signByBorrower(uint _loanId) public isLoanBorrower(_loanId) isValidLoan(_loanId) isLoanIn(_loanId, LoanState.INSURANCE_APPROVED)
    
Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isLoanBorrower()`` - The function caller should be the Borrower of the Loan.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.INSURANCE_APPROVED)`` - Checks Loan is in INSURANCE_APPLIED state.

approveLoan(...)
^^^^^^^^^^^^^^^^

This function changes the ``bankApprove`` value to ``True`` and change the Loan state to ``BANK_APPROVED`` state. ::
    
    function approveLoan(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.BORROWER_SIGNED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.BORROWER_SIGNED)`` - Checks Loan is in BORROWER_SIGNED state.

rejectLoan(...)
^^^^^^^^^^^^^^^

This function changes the ``bankApprove`` value to ``False`` and change the Loan state to ``BANK_REJECTED`` state. ::

    function rejectLoan(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.BORROWER_SIGNED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.BORROWER_SIGNED)`` - Checks Loan is in BORROWER_SIGNED state.

confirmTokenTrasferToInsurance(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This function changes the Loan state to PAID_TO_INSURANCE. ::

    function confirmTokenTrasferToInsurance(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.BANK_APPROVED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.BANK_APPROVED)`` - Checks Loan is in BANK_APPROVED state.

confirmTokenTrasferToBroker(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This function changes the Loan state to PAID_TO_BROKER. ::

    function confirmTokenTrasferToBroker(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.PAID_TO_INSURANCE)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.PAID_TO_INSURANCE)`` - Checks Loan is in PAID_TO_INSURANCE state.

confirmTokenTrasferToBorrower(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This function changes the Loan state to ONGOING. ::

    function confirmTokenTrasferToBorrower(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.PAID_TO_BROKER)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.PAID_TO_BROKER)`` - Checks Loan is in PAID_TO_BROKER state.

closeLoan(...)
^^^^^^^^^^^^^^ 

This function changes the Loan state to CLOSE. ::

    function closeLoan(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.ONGOING)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.ONGOING)`` - Checks Loan is in ONGOING state.

markAsDefaulted(...)
^^^^^^^^^^^^^^^^^^^^

This function changes the Loan state to DEFAULT. ::

    function markAsDefaulted(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.ONGOING)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.ONGOING)`` - Checks Loan is in ONGOING state.

requestClaim(...) 
^^^^^^^^^^^^^^^^^

This function changes the Loan state to CLAIM_REQUESTED. ::

    function requestClaim(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.DEFAULT)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.DEFAULT)`` - Checks Loan is in DEFAULT state.

confirmRecivingOfClaim(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^ 

This function changes the Loan state to CLAIMED. ::

    function confirmRecivingOfClaim(uint _loanId) public isAdmin() isValidLoan(_loanId) isLoanIn(_loanId, LoanState.CLAIM_REQUESTED)

Parameters:
    * ``_loanId`` -  Loan Id

Modifiers:
    * ``isAdmin()`` - The function caller should be the Bank.
    * ``isValidLoan(_loanId)`` - Checks Loan validity
    * ``isLoanIn(_loanId, LoanState.CLAIM_REQUESTED)`` - Checks Loan is in CLAIM_REQUESTED state.

viewLoan(...)
^^^^^^^^^^^^^

This function returns the Loan. ::

    function viewLoan(uint _loanId) public view returns(Loan memory loan)

Parameters:
    * ``_loanId`` -  Loan Id

Return:
    * ``loan`` - Loan


getLoans()
^^^^^^^^^^^^^

This function returns all the Loans. ::

    function getLoans() public view returns(Loan [] memory)

Return:
    * ``Loan []`` - All Loans as an array.


Insurance Policy Smart Contract
-------------------------------

This smart contract stores the Insurance Policy data applied for Loans. 
The Insurance Company deploys this smart contract and it is the owner of this smart contract.
The following sections describe the components of the smart contract.

State Transition Diagram of The Insurance Policy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following diagram shows the state transition of a Insurance Policy.
We follow this state transition diagram to implement the Insurance Policy state changes in ``InsurancePolicy`` smart contract.

.. image:: ../images/state_transition_insurance.png

ENUM
~~~~

1. InsuranceState - This enum holds individual Insurance Policy states. There are 11 Insurance Policy states. ::

    enum InsuranceState{
        REQUESTED, 
        BORROWER_SIGNED,
        APPROVED, 
        REJECTED, 
        ONGOING,
        CLAIM_REQUESTED,
        CLAIM_APPROVED,
        CLAIM_REJECTED,
        CLAIMED, 
        CLOSE
    }

* ``REQUESTED`` - Initial state of a Insurance Policy. Broker request a insurance policy foe the Bank Loan. 
* ``BORROWER_SIGNED`` - Borrower agreed for the Insurance Policy and signed the contract.
* ``APPROVED`` - Insurance Company approved the Insurance Policy request.
* ``REJECTED`` - Insurance Company rejected the Insurance Policy request.
* ``ONGOING`` - Bank paid the initial payment. Loan started. Insurance policy covering the Loan.
* ``CLAIM_REQUESTED`` - Bank request a claim for the defaulted Loan.
* ``CLAIM_APPROVED`` - Insurance Company approved the claim request made by the Bank.
* ``CLAIM_REJECTED`` - Insurance Comapny rejected the claim request.
* ``CLAIMED`` - Insurance Company paid the claim to the Bank. 
* ``CLOSE`` - Close the Insurance Policy. No longer covering the Bank Loan.


Structs
~~~~~~~

1. Policy - This Policy struct holds the Insurance Policy attributes. ::
    
    struct Policy
    {
        uint id;
        uint amount;
        uint months;
        uint cover;
        uint planId;
        uint loanId;
        InsuranceState state;
        address broker;
        address borrower;
        bool insuranceApprove;
        bool isBorrowerSigned;
    }

* ``id`` - Insurance policy Id.
* ``amount`` - Insurance policy fee.
* ``months`` - Insurance validity period.
* ``cover`` - Loan amount covered by the Insurance.
* ``planId`` - Insurance plan id.
* ``loanId`` - Bank Loan Id.
* ``state`` - Current state of the Insurance Policy 
* ``broker`` - Broker of the Bank Loan/ Insurance Policy
* ``borrower`` - Borrower of the Bank Loan.
* ``insuranceApprove`` - Insurance Company approval status for the Insurance Policy.
* ``isBorrowerSigned`` - Borrower signed status.

Events
~~~~~~

Events defined in the ``InsurancePolicy.sol`` smart contract.

insurancePolicyRequest
^^^^^^^^^^^^^^^^^^^^^^

This event will emit when Broker create a Insurance Policy request. ::

    event insurancePolicyRequest(
        uint id,
        uint amount,
        uint months,
        uint cover,
        uint planId,
        uint loanId,
        InsuranceState state,
        address broker,
        address borrower,
        bool insuranceApprove,
        bool isBorrowerSigned
    );

Parameters: 
    * ``id`` - Insurance policy Id.
    * ``amount`` - Insurance policy fee.
    * ``months`` - Insurance validity period.
    * ``cover`` - Loan amount covered by the Insurance.
    * ``planId`` - Insurance plan id.
    * ``loanId`` - Bank Loan Id.
    * ``state`` - Current state of the Insurance Policy 
    * ``broker`` - Broker of the Bank Loan/ Insurance Policy
    * ``borrower`` - Borrower of the Bank Loan.
    * ``insuranceApprove`` - Insurance Company approval status for the Insurance Policy.
    * ``isBorrowerSigned`` - Borrower signed status.

Modifiers
~~~~~~~~~

The following modifiers are used in the ``InsurancePolicy.sol`` functions.

* ``isAdmin()`` - Checks the function callers is the owner of the smart contract.
* ``isBorrower(address _address)`` - Checks the ``_address`` registered as a Borrower in the ``UserIdentity`` smart contract.
* ``isBroker(address _address)`` - Checks the ``_address`` registered as a Broker in the ``UserIdentity`` smart contract
* ``isBank(address _address) - Checks the ``_address`` is the Bank wallet address.
* ``isValidInsurance(uint _insuranceId)`` - Checks Insurance Policy exists in the system.
* ``isInsuranceIn(uint _policyId, InsuranceState _state)`` - Check the insurance policy registered under the ``_policyId`` is in ``_state``.
* ``checkBorrower(uint _policyId, address _borrower)`` - Checks the Borrower registered in the ``_policyId`` is equal to ``_borrower`` address.
    
Attributes
~~~~~~~~~~

1. ``UserIdentity: identitySC`` - Stores UserIdentity smart contract object
2. ``address private admin`` - Store smart contract deployer’s address.
3. ``Policy[] private policies`` - Stores insurance policy data

Constructor
~~~~~~~~~~~

``InsurancePolicy.sol`` takes ``UserIdentity.sol`` address as a parameter for the constructor function.
It sets the deployer's address as the smart contract ``admin`` address. ::

    constructor (address _identitySC) {
            admin = msg.sender;
            identitySC = UserIdentity(_identitySC);
    }

Functions
~~~~~~~~~

applyInsurance(...)
^^^^^^^^^^^^^^^^^^^

This function creates a Insurance Policy request. ::

    function applyInsurance(uint _amount, uint _months, uint _initialPayment, uint _finalPayment, string memory _planId, uint _loanId, address _borrower)
        public isBroker(msg.sender) isBorrower(_borrower)

Parameters:
    * ``_amount`` - Loan amount.
    * ``_months`` - Duration of the Loan.
    * ``_initialPayment`` - Initial payment for the Insuranec Policy. Paid by Bank.
    * ``_finalPayment`` - Final payment for the Insurance Policy. Paid by Borrower.
    * ``_planId`` - Insurance Plan Id.
    * ``_loanId`` - Bank Loan Id.
    * ``_borrower`` - Borrower of the Loan.

Modifiers:
    * ``isBroker(msg.sender)`` - The function caller registered as a Broker.
    * ``isBorrower(_borrower)`` - The ``_borrower`` registered as a Borrower.

signInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^

This function sets the Insurance Policy attribute ``isBorrowerSigned`` to ``True`` and 
changes the Insurance Policy state to BORROWER_SIGNED. ::

    function signInsurancePolicy(uint _policyId) 
        public checkBorrower(_policyId, msg.sender) isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.REQUESTED)
    
Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``checkBorrower(_policyId, msg.sender)`` - Check function caller is the registered Borrower of the Insurance Policy.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.REQUESTED)`` - Check Insurance Policy is in the REQUESTED state.

approveInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^
    
This function changes the Insurance Policy attribute ``insuranceApprove`` value to ``True`` and 
changes the Insurance Policy state to APPROVED. ::

    function approveInsurancePolicy(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.BORROWER_SIGNED)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.BORROWER_SIGNED)`` - Check Insurance Policy is in the BORROWER_SIGNED state.
    
rejectInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy attribute ``insuranceApprove`` value to ``False`` and 
changes the Insurance Policy state to REJECTED. ::

    function rejectInsurancePolicy(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.BORROWER_SIGNED)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.BORROWER_SIGNED)`` - Check Insurance Policy is in the BORROWER_SIGNED state.

confirmPaymentFromBank(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to ONGOING. ::

    function confirmPaymentFromBank(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.APPROVED)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.APPROVED)`` - Check Insurance Policy is in the APPROVED state.


confirmPaymentFromBorrower(...)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  

This function changes the Insurance Policy state to BORROWER_PAID. ::

    function confirmPaymentFromBorrower(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.ONGOING)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.ONGOING)`` - Check Insurance Policy is in the ONGOING state.
    
closePolicy(...)
^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLOSE. ::

    function closePolicy(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.BORROWER_PAID)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.BORROWER_PAID)`` - Check Insurance Policy is in the BORROWER_PAID state.

requestClaim(...)
^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIM_REQUESTED. ::
    
    function requestClaim(uint _policyId) 
        public isBank(msg.sender) isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.ONGOING)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.ONGOING)`` - Check Insurance Policy is in the ONGOING state.

rejectClaim(...)
^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIM_REJECTED. ::
    
    function rejectClaim(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.CLAIM_REQUESTED)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.CLAIM_REQUESTED)`` - Check Insurance Policy is in the CLAIM_REQUESTED state.

approveClaim(...)
^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIM_APPROVED. ::
    
    function approveClaim(uint _policyId) 
        public isAdmin() isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.CLAIM_REQUESTED)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.CLAIM_REQUESTED)`` - Check Insurance Policy is in the CLAIM_REQUESTED state.

confirmClaim(...)
^^^^^^^^^^^^^^^^^

This function changes the Insurance Policy state to CLAIMED. ::

    function confirmClaim(uint _policyId) 
        public isBank(msg.sender) isValidInsurance(_policyId) isInsuranceIn(_policyId, InsuranceState.CLAIM_APPROVED)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isAdmin()`` - Checks the function caller is the Admin of the smart contract.
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.
    * ``isInsuranceIn(_policyId, InsuranceState.CLAIM_APPROVED)`` - Check Insurance Policy is in the CLAIM_APPROVED state.


viewInsurancePolicy(...)
^^^^^^^^^^^^^^^^^^^^^^^^

This function returns the requested Insurance Policy details. ::
    
    function viewInsurancePolicy(uint _policyId) public view isAdmin() isValidInsurance(_policyId) 
        returns(Policy memory policy)

Parameters:
    * ``_policyId`` - Insurance Policy Id.

Modifiers:
    * ``isValidInsurance(_policyId)`` - Check Insurance Policy exists.

Return: 
    * ``Loan`` - Return Insurance Policy registered under ``_policyId``.

    
getAllPolicies(...)
^^^^^^^^^^^^^^^^^^^

This function returns all the Loans. ::

    function getAllPolicies() public view returns(Policy [] memory)

Return: 
    * ``Loan []`` - Return all Insurance Policies as an object array.