const Express = require('express')
const RentPayment = require('../models/RentPayment');

const router = Express.Router()
const rentPaymentService = require('../services/rentPaymentService');

//GET ALL Rent Payments
/**
 * @swagger
 * components:
 *  schemas:
 *      RentPayment:
 *          type: object
 *          required:
 *              -borrower
 *              -rentId
 *              -amount
 *              -transactionHash
 *          properties:
 *              _id:
 *                  type: string
 *                  description: The auto generated id from Mongo DB
 *              borrower:
 *                  type: string
 *                  description: Borrower address
 *              rentId:
 *                  type: number
 *                  description: Rent id
 *              amount:
 *                  type: number
 *                  description: Paid amount
 *              transactionHash:
 *                  type: sring
 *                  description: Blockchain transaction hash
 *          example:
 *              _id: 60d6ffbcc743bb4d6c69da68
 *              borrower: '0x940028a249EB48446dA6E68DD9A1927Cd4822A9f'
 *              rentId: 1
 *              amount: 300
 *              transactionHash: '0x0025a9562a86021ec187a33d6c3ad65a5ee3538b52e383769fec675fd500387d'
 *              
 */


/**
 * @swagger
 * tags:
 *  name: Rent Payments
 *  description: The Rent Payment API for the Microfinance
 */

/**
 * @swagger
 * /rent-payments:
 *  get:
 *      summary: Returns the list of all rent payment transactions
 *      tags: [Rent Payments]
 *      responses:
 *          200:
 *              description: The list of the rent plans
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/RentPayment'
 */
router.get('/', async (req, res) => {
    try {
		const rentPayments = await rentPaymentService.getRentPayments();
		res.json(rentPayments);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//GET SPECIFIC PAYMENT DETAILS

/**
 * @swagger
 * /rent-payments/{paymentId}:
 *  get:
 *      summary: Get rent payment by id
 *      tags: [Rent Payments]
 *      parameters:
 *          - in: path
 *            name: paymentId
 *            schema:
 *              type: string
 *            required: true
 *            desciption: The rent payment id
 *      responses:
 *          200:
 *              description: The rent payment by id
 *              contents:
 *                  application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/RentPayment'
 *          404:
 *              description: The rent payment was not found
 *                      
 *                  
 *          
 */
 router.get('/:paymentId', async (req, res) => {
    console.log(req.params.paymentId);
    try{
		const rentPayments = await rentPaymentService.getRentPaymentById(req);
		res.json(rentPayments);
    }
    catch(err){
        res.json({
            message: err
        })
    }
    
})

// SUBMIT A RENT PAYMENT

/**
 * @swagger
 * /rent-payments:
 *  post:
 *      summary: Add new rent payment entry
 *      tags: [Rent Payments]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/RentPayment'
 *      responses:
 *          200:
 *              description: The rent payment was successfully created
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/RentPayment'
 *          500:
 *              description: Some server error
 */
 router.post('/', async (req, res) => {
    try {
		const rentPayment = await rentPaymentService.saveRentPayment(req);
		res.json(rentPayment);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//UPDATE RENT PAYMENT

/**
 * @swagger
 * /rent-payments/{paymentId}:
 *  patch:
 *      summary: Update the rent payment by Id
 *      tags: [Rent Payments]
 *      parameters:
 *          - in: path
 *            name: paymentId
 *            schema:
 *              type: string
 *            required: true
 *            description: The rent payment id
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/RentPayment'
 *      responses:
 *          200:
 *              description: The rent payment was successfully created
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/RentPayment'
 *          404:
 *              description: The rent payment is not found
 *          500:
 *              description: Some server error
 */
 router.patch('/:paymentId', async (req, res) => {
    console.log(req.params.paymentId);
    try {
        const updatedPayment = await rentPaymentService.updateRentPayment(req);
        res.json(updatedPayment);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//DELETE RENT PAYMENT

/**
 * @swagger
 * /rent-payments/{paymentId}:
 *  delete:
 *      summary: Remove the rent payment by Id
 *      tags: [Rent Payments]
 *      parameters:
 *          - in: path
 *            name: paymentId
 *            schema:
 *              type: string
 *            required: true
 *            description: The rent payment id
 *      responses:
 *          200:
 *              description: The rent payment was successfully deleted
 *          404:
 *              description: The rent payment is not found
 *          500:
 *              description: Some server error
 */
 router.delete('/:paymentId', async (req, res) => {
    console.log(req.params.paymentId);
    try {
        const payment = await rentPaymentService.deleteRentPayment(req);
        if(payment.deletedCount==0){
            res.status(404).send('Rent Payment not found');
        }
        res.json(payment);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

module.exports = router;
