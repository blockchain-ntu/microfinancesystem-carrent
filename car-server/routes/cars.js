const Express = require('express')
const Car = require('../models/Cars')

const router = Express.Router()

const carService = require('../services/carService');

//GET ALL Cars

/**
 * @swagger
 * components:
 *  schemas:
 *      Car:
 *          type: object
 *          required:
 *              -model
 *              -licensePlate
 *              -pricePerDay
 *          
 *          properties:
 *              _id:
 *                  type: string
 *                  description: The auto generated id from Mongo DB
 *              model:
 *                  type: String
 *                  description: Car Model
 *              licensePlate:
 *                  type: String
 *                  description: Car License
 *              pricePerDay:
 *                  type: number
 *                  description: rental fee per model
 *              
 *          example:
 *              _id: 60d6ffbcc743bb4d6c69da68
 *              model: "Toyota"
 *              licensePlate: "SGD6542I"
 *              pricePerDay: 300
 *              
 */

/**
 * @swagger
 * tags:
 *  name: Car
 *  description: The Car API for the Microfinance
 */

/**
 * @swagger
 * /cars:
 *  get:
 *      summary: Returns the list of all cars
 *      tags: [Cars]
 *      responses:
 *          200:
 *              description: The list of the cars
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items: 
 *                              $ref: '#/components/schemas/Car'
 */
router.get('/', async (req, res) => {
    try {
		const cars = await carService.getCars();
		res.json(cars);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})



//GET SPECIFIC CARS

/**
 * @swagger
 * /cars/{carId}:
 *  get:
 *      summary: Get cars by id
 *      tags: [Cars]
 *      parameters:
 *          - in: path
 *            name: carId
 *            schema:
 *              type: string
 *            required: true
 *            desciption: The car id
 *      responses:
 *          200:
 *              description: The car by id
 *              contents:
 *                  application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Car'
 *          404:
 *              description: The Car was not found
 *                      
 *                  
 *          
 */
router.get('/:carId', async (req, res) => {
    try{
        const cars = await carService.getCarById(req);
        res.json(cars);
    }
    catch(err){
        res.json({
            message: err
        })
    }
    
})

// SUBMIT A CAR

/**
 * @swagger
 * /cars:
 *  post:
 *      summary: Create a car
 *      tags: [Cars]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Car'
 *      responses:
 *          200:
 *              description: The Car was successfully created
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Car'
 *          500:
 *              description: Some server error
 */
router.post('/', async (req, res) => {
    try {
        const savedCar = await carService.createCar(req);
        res.json(savedCar);
    }
    catch (err) {
        res.json({
            message: err
        })
    }


})

//UPDATE CAR

/**
 * @swagger
 * /cars/{carId}:
 *  patch:
 *      summary: Update the car by Id
 *      tags: [Cars]
 *      parameters:
 *          - in: path
 *            name: carId
 *            schema:
 *              type: string
 *            required: true
 *            description: The car id
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/car'
 *      responses:
 *          200:
 *              description: The car was successfully created
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Car'
 *          404:
 *              description: The car is not found
 *          500:
 *              description: Some server error
 */
 router.patch('/:carId', async (req, res) => {
    try {
        const updatedCar = await carService.updateCar(req)
        res.json(updatedCar);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

//DELETE Car

/**
 * @swagger
 * /cars/{carId}:
 *  delete:
 *      summary: Remove the car by Id
 *      tags: [Loan Plans]
 *      parameters:
 *          - in: path
 *            name: carId
 *            schema:
 *              type: string
 *            required: true
 *            description: The car id
 *      responses:
 *          200:
 *              description: The car was successfully deleted
 *          404:
 *              description: The car is not found
 *          500:
 *              description: Some server error
 */
router.delete('/:carId', async (req, res) => {
    try {
        const cars = await carService.deleteCar(req);
        if(cars.deletedCount==0){
            res.status(404).send('Car not found');
        }
        res.json(cars);
    }
    catch (err) {
        res.json({
            message: err
        })
    }
})

module.exports = router;