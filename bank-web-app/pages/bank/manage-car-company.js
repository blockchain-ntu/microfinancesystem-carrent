import React from 'react';
import { Row, Col } from 'antd';
import CarOwnerTable from '../../components/userManagement/CarOwnerTable';

function ManageCarOwners() {
	return (
		<Row gutter={[16, 16]}>
			<Col span={24}>
				<CarOwnerTable />
			</Col>
		</Row>
	);
}

export default CarOwnerTable;
