import React, { useState, useContext, useEffect } from 'react';
import { Table, Tag, Card, Space, Button, message } from 'antd';
import SmartContractContext from '../../stores/smartContractContext';
import UserContext from '../../stores/userContext';

function CarOwnerTable() {
	const { user } = useContext(UserContext);
	const { UserIdentityContract } = useContext(SmartContractContext);
	const [data, setData] = useState([]);

	const getCarOwners = async () => {
		try {
			const response = await UserIdentityContract.methods.getAllCarOwners().call();

			setData([]);

			for (let i = 0; i < response.length; i++) {
				const row = {
					key: response[i].id,
					id: response[i].id,
					socialId: response[i].socialSecurityId,
					address: response[i].walletAddress,
					name: response[i].name,
					status: response[i].state,
				};

				setData((prev) => {
					return [...prev, row];
				});
			}
		} catch (err) {
			console.log(err);
			message.error('Error occured while loading car owners');
		}
	};

	const approveCarOwner = async (address) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.approveCarOwner(address).send({ from: accounts[0] });
			message.success('Car Owner approved successfully!');
			getCarOwners();
		} catch (err) {
			message.error('Error approving Car Owner');
		}
	};

	const rejectCarOwner = async (address) => {
		try {
			const accounts = await window.ethereum.enable();
			await UserIdentityContract.methods.rejectCarOwner(address).send({ from: accounts[0] });
			message.success('Car Owner request rejected');
			getCarOwners();
		} catch (err) {
			message.error('Error occured while rejecting car owner');
		}
	};

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: text => text,
		},
		{
			title: 'Social Id',
			dataIndex: 'socialId',
			key: 'socialId',
		},
		{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Address',
			dataIndex: 'address',
			key: 'address',
		},
		{
			title: 'Status',
			key: 'status',
			dataIndex: 'status',
			render: tag => {
				const s = ['PENDING', 'APPROVED', 'REJECTED'];
				const c = ['geekblue', 'green', 'red'];
				return (
					<Tag color={c[tag]} key={tag}>
						{s[tag]}
					</Tag>
				);
			},
		},
	];

	if (user.role === 'bank') {
		columns.push({
			title: 'Action',
			dataIndex: '',
			key: 'x',
			render: (record) => (
				record.status === '0' ?
					<Space>
						<Button type="primary" ghost onClick={() => approveCarOwner(record.address)}>Approve</Button>
						<Button type="primary" danger ghost style={{ color: 'red' }} onClick={() => rejectCarOwner(record.address)}>Reject</Button>
					</Space> : null
			),
		});
	}

	useEffect(() => {
		getCarOwners();
		// TODO: add event listner for newUserAdded event.
	}, []);

	return (
		<Card title="Car Owners">
			<Table pagination="True" columns={columns} dataSource={data} />
		</Card>
	);
}

export default CarOwnerTable;
